import { List } from "immutable";
import { isObject } from "util";

export interface IErrorJSON<T extends {}> {
  errors: Record<keyof T, string[]>;
}

export function isErrorJSON<T>(value: any): value is IErrorJSON<T> {
  return value && value.errors && isObject(value.errors);
}

export function flattenErrors<T extends {}>(
  error?: any
): List<[keyof T, string]> {
  if (isErrorJSON<T>(error)) {
    return Object.entries<string[]>(error.errors).reduce(
      (acc, [field, errors]) =>
        errors.reduce((acc, error) => acc.push([field as any, error]), acc),
      List<[keyof T, string]>()
    );
  } else {
    return List();
  }
}
