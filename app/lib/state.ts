import { State } from "@aicacia/state";
import { INITIAL_STATE as forms } from "@aicacia/state-forms";
import { createContext } from "@aicacia/state-react";
import { INITIAL_STATE as connections } from "../stores/connections/definitions";
import { INITIAL_STATE as cronJobs } from "../stores/cronJobs/definitions";
import { INITIAL_STATE as extractors } from "../stores/extractors/definitions";
import { INITIAL_STATE as locales } from "../stores/lib/locales/definitions";
import { INITIAL_STATE as router } from "../stores/lib/router/definitions";
import { INITIAL_STATE as screenSize } from "../stores/lib/screenSize/definitions";
import { INITIAL_STATE as mappers } from "../stores/mappers/definitions";
import { INITIAL_STATE as sidebar } from "../stores/sidebar/definitions";
import { INITIAL_STATE as sync } from "../stores/sync/definitions";
import { INITIAL_STATE as tables } from "../stores/tables/definitions";

export const INITIAL_STATE = {
  sync,
  forms,
  locales,
  connections,
  sidebar,
  mappers,
  extractors,
  tables,
  cronJobs,
  router,
  screenSize
};
export const state = new State(INITIAL_STATE);
export type IState = ReturnType<typeof state["getState"]>;

export const { Provider, Consumer, connect } = createContext<IState>(
  state.getState()
);

if (process.env.NODE_ENV !== "production") {
  (window as any).__DEV_STATE = state;

  if ((window as any).__REDUX_DEVTOOLS_EXTENSION__) {
    const devTools = (window as any).__REDUX_DEVTOOLS_EXTENSION__.connect();

    devTools.subscribe((message: any) => {
      if (
        message.type === "DISPATCH" &&
        message.payload.type === "JUMP_TO_ACTION"
      ) {
        state.setStateJSON(JSON.parse(message.state));
      }
    });

    state.on("set-state-for", name => {
      devTools.send(
        {
          type: name || "unknown",
          payload: state.getStateFor(name)
        },
        state.getState()
      );
    });
  }
}
