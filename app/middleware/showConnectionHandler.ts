import { IHandler } from "@aicacia/router";
import { parse } from "url";
import { CONNECTION_ID, showConnection } from "../stores/connections";
import { IRouterContext } from "../stores/lib/router";

export const showConnectionHandler: IHandler = (context: IRouterContext) =>
  showConnection(+context.params[CONNECTION_ID])
    .catch(error => {
      console.error(error);
      context.redirectUrl = parse("/404", true);
      return context;
    })
    .then(() => context);
