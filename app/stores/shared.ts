export const UPDATE_DEBOUNCE_MS = 1000;

export enum ConnectionType {
  NONE = "none",
  POSTGRESQL = "postgresql"
}

export enum TableType {
  ID = "id",
  BINARY_ID = "binary_id",
  STRING = "string",
  INTEGER = "integer",
  DATE = "date",
  ARRAY = "array",
  MAP = "map"
}

export const TABLE_TYPES = Object.values(TableType);
