import { List, Map, Record } from "immutable";
import {
  extractorByIdUrl,
  EXTRACTORS_BY_ID_URL
} from "../extractors/definitions";

export interface IMapper {
  id: number;
  table_id: number;
  extractor_id: number;
  name: string;
  fields: List<Record<IMapperField>>;
  inserted_at: string;
  updated_at: string;
}

export const Mapper = Record<IMapper>({
  id: 0,
  table_id: 0,
  extractor_id: 0,
  name: "",
  fields: List(),
  inserted_at: new Date().toJSON(),
  updated_at: new Date().toJSON()
});

export interface IMapperField {
  id: number;
  mapper_id: number;
  from: string;
  to_id: number;
  inserted_at: string;
  updated_at: string;
}

export const MapperField = Record<IMapperField>({
  id: 0,
  mapper_id: 0,
  from: "",
  to_id: 0,
  inserted_at: new Date().toJSON(),
  updated_at: new Date().toJSON()
});

export interface IMappers {
  byId: Map<number, Record<IMapper>>;
}

export const Mappers = Record<IMappers>({
  byId: Map()
});

export type IMapperFieldJSON = ReturnType<Record<IMapperField>["toJS"]>;
export interface IMapperJSON extends ReturnType<Record<IMapper>["toJS"]> {
  fields: IMapperFieldJSON[];
}
export interface IMappersJSON {
  byId: { [id: number]: IMapperJSON };
}

export const MAPPERS = `mappers`;
export const MAPPER_ID = "mapper_id";
export const MAPPERS_URL = `${EXTRACTORS_BY_ID_URL}/${MAPPERS}`;
export const MAPPERS_BY_ID_URL = `${MAPPERS_URL}/${MAPPER_ID}`;

export const mappersUrl = (connectionId: number, extractorId: number) =>
  `${extractorByIdUrl(connectionId, extractorId)}/${MAPPERS}`;
export const mapperByIdUrl = (
  connectionId: number,
  extractorId: number,
  mapperId: number
) => `${mappersUrl(connectionId, extractorId)}/${mapperId}`;

export const INITIAL_STATE = Mappers();
export const STORE_NAME = "mappers";
