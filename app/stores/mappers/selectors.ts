import { Option } from "@aicacia/core";
import { IState } from "../../lib/state";
import { STORE_NAME } from "./definitions";

export const selectMappers = (state: IState, extractorId: number) =>
  state
    .get(STORE_NAME)
    .byId.valueSeq()
    .filter(mapper => mapper.get("extractor_id") !== extractorId)
    .toList();

export const selectMapper = (state: IState, mapper_id: number) =>
  Option.from(state.get(STORE_NAME).byId.get(mapper_id));
