import { none, Option, some } from "@aicacia/core";
import { debounce } from "@aicacia/debounce";
import { IJSON } from "@aicacia/json";
import Axios from "axios";
import { List, Map, Record } from "immutable";
import { state } from "../../lib/state";
import { UPDATE_DEBOUNCE_MS } from "../shared";
import { endSync, startSync } from "../sync";
import {
  IMapper,
  IMapperJSON,
  IMappersJSON,
  Mapper,
  mapperByIdUrl,
  MapperField,
  Mappers,
  mappersUrl,
  STORE_NAME
} from "./definitions";

export const store = state.getStore(STORE_NAME);

export const MapperFromJSON = (mapperJSON: IMapperJSON) =>
  Mapper({
    ...mapperJSON,
    fields: List(mapperJSON.fields.map(MapperField))
  });

store.fromJSON = (json: IJSON) => {
  const mappersJSON: IMappersJSON = json as any;

  return Mappers({
    byId: Object.values(mappersJSON.byId).reduce(
      (byId, mapperJSON) => byId.set(mapperJSON.id, MapperFromJSON(mapperJSON)),
      Map<number, Record<IMapper>>()
    )
  });
};

export const allMappers = (connectionId: number, extractorId: number) =>
  Axios.get<IMapperJSON[]>(mappersUrl(connectionId, extractorId))
    .catch(error => Promise.reject(error.response.data))
    .then(response => {
      const mappers = List(response.data.map(MapperFromJSON));

      store.updateState(state =>
        state.set(
          "byId",
          mappers.reduce(
            (byId, mapper) => byId.set(mapper.get("id"), mapper),
            state.get("byId")
          )
        )
      );

      return mappers;
    });

export const showMapper = (
  connectionId: number,
  extractorId: number,
  mapperId: number
) => {
  const mapper = store
    .getState()
    .get("byId")
    .get(mapperId);

  if (mapper) {
    return Promise.resolve(mapper);
  } else {
    return Axios.get<IMapperJSON>(
      mapperByIdUrl(connectionId, extractorId, mapperId)
    )
      .catch(error => Promise.reject(error.response.data))
      .then(response => {
        const mapper = MapperFromJSON(response.data);

        store.updateState(state =>
          state.set("byId", state.get("byId").set(mapper.get("id"), mapper))
        );

        return mapper;
      });
  }
};

export const createMapper = (
  connectionId: number,
  extractorId: number,
  mapper: Partial<IMapperJSON>
) =>
  Axios.post<IMapperJSON>(mappersUrl(connectionId, extractorId), mapper)
    .catch(error => Promise.reject(error.response.data))
    .then(response => {
      const mapper = MapperFromJSON(response.data);

      store.updateState(state =>
        state.set("byId", state.get("byId").set(mapper.get("id"), mapper))
      );

      return mapper;
    });

export const updateMapper = (
  connectionId: number,
  extractorId: number,
  mapperId: number,
  mapper: Partial<IMapperJSON>
) =>
  Axios.patch<IMapperJSON>(
    mapperByIdUrl(connectionId, extractorId, mapperId),
    mapper
  )
    .catch(error => Promise.reject(error.response.data))
    .then(response => {
      const mapper = MapperFromJSON(response.data);

      store.updateState(state =>
        state.set("byId", state.get("byId").set(mapper.get("id"), mapper))
      );

      return mapper;
    });

export const createDebounceUpdateMapper = (): ((
  callbackFn: (updateFn: typeof updateMapper) => void
) => void) =>
  debounce(onDebounce => onDebounce(updateMapper), UPDATE_DEBOUNCE_MS, {
    before: startSync,
    after: endSync
  });

export const deleteMapper = (
  connectionId: number,
  extractorId: number,
  mapperId: number
): Promise<Option<Record<IMapper>>> => {
  const mapper = store
    .getState()
    .get("byId")
    .get(mapperId);

  if (mapper) {
    store.updateState(state =>
      state.set("byId", state.get("byId").delete(mapper.get("id")))
    );

    return Axios.delete<IMapperJSON>(
      mapperByIdUrl(connectionId, extractorId, mapperId)
    )
      .catch(error => Promise.reject(error.response.data))
      .then(() => some(mapper))
      .catch(() => {
        store.updateState(state =>
          state.set("byId", state.get("byId").set(mapper.get("id"), mapper))
        );
        return none();
      });
  } else {
    return Promise.reject(none());
  }
};
