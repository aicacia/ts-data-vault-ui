import { Option } from "@aicacia/core";
import { IState } from "../../lib/state";
import { STORE_NAME } from "./definitions";

export const selectExtractors = (state: IState, connectionId: number) =>
  state
    .get(STORE_NAME)
    .byId.valueSeq()
    .filter(extractor => extractor.get("connection_id") !== connectionId)
    .toList();

export const selectExtractor = (state: IState, extractor_id: number) =>
  Option.from(state.get(STORE_NAME).byId.get(extractor_id));
