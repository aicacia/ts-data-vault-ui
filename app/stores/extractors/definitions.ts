import { Map, Record } from "immutable";
import {
  connectionByIdUrl,
  CONNECTIONS_BY_ID_URL
} from "../connections/definitions";
import { ConnectionType } from "../shared";

export interface IBaseExtractor {
  id: number;
  connection_id: number;
  name: string;
  type: ConnectionType;
  inserted_at: string;
  updated_at: string;
}

export interface IPostgresqlExtractor extends IBaseExtractor {
  query: string;
}

export const PostgresqlExtractor = Record<IPostgresqlExtractor>({
  id: 0,
  connection_id: 0,
  name: "",
  type: ConnectionType.POSTGRESQL,
  query: "",
  inserted_at: new Date().toJSON(),
  updated_at: new Date().toJSON()
});

export type IExtractor = IPostgresqlExtractor;

export interface IExtractors {
  byId: Map<number, Record<IExtractor>>;
}

export const Extractors = Record<IExtractors>({
  byId: Map()
});

export type IPostgresqlExtractorJSON = ReturnType<
  Record<IPostgresqlExtractor>["toJS"]
>;
export type IExtractorJSON = ReturnType<Record<IExtractor>["toJS"]>;
export interface IExtractorsJSON {
  byId: { [id: number]: IExtractorJSON };
}

export function isPostgresqlExtractor(
  extractor: Record<IExtractor>
): extractor is Record<IPostgresqlExtractor> {
  return extractor.get("type") === ConnectionType.POSTGRESQL;
}

export function isPostgresqlExtractorJSON(
  extractorJSON: IExtractorJSON
): extractorJSON is IPostgresqlExtractorJSON {
  return extractorJSON.type === ConnectionType.POSTGRESQL;
}

export const EXTRACTORS = `extractors`;
export const EXTRACTOR_ID = "extractor_id";
export const EXTRACTORS_URL = `${CONNECTIONS_BY_ID_URL}/${EXTRACTORS}`;
export const EXTRACTORS_BY_ID_URL = `${EXTRACTORS_URL}/${EXTRACTOR_ID}`;

export const extractorsUrl = (connectionId: number) =>
  `${connectionByIdUrl(connectionId)}/${EXTRACTORS}`;
export const extractorByIdUrl = (connectionId: number, extractorId: number) =>
  `${extractorsUrl(connectionId)}/${extractorId}`;

export const INITIAL_STATE = Extractors();
export const STORE_NAME = "extractors";
