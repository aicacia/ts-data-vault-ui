import { none, Option, some } from "@aicacia/core";
import { debounce } from "@aicacia/debounce";
import { IJSON } from "@aicacia/json";
import Axios from "axios";
import { List, Map, Record } from "immutable";
import { state } from "../../lib/state";
import { UPDATE_DEBOUNCE_MS } from "../shared";
import { endSync, startSync } from "../sync";
import {
  extractorByIdUrl,
  Extractors,
  extractorsUrl,
  IExtractor,
  IExtractorJSON,
  IExtractorsJSON,
  IPostgresqlExtractor,
  isPostgresqlExtractorJSON,
  PostgresqlExtractor,
  STORE_NAME
} from "./definitions";

export const store = state.getStore(STORE_NAME);

export const ExtractorFromJSON = (extractorJSON: IExtractorJSON) => {
  if (isPostgresqlExtractorJSON(extractorJSON)) {
    return PostgresqlExtractor(extractorJSON);
  } else {
    throw new TypeError(
      "Invalid extractor type " + JSON.stringify(extractorJSON)
    );
  }
};

store.fromJSON = (json: IJSON) => {
  const extractorsJSON: IExtractorsJSON = json as any;

  return Extractors({
    byId: Object.values(extractorsJSON.byId).reduce(
      (byId, extractorJSON) =>
        byId.set(extractorJSON.id, ExtractorFromJSON(extractorJSON)),
      Map<number, Record<IExtractor>>()
    )
  });
};

export const allExtractors = (connectionId: number) =>
  Axios.get<IExtractorJSON[]>(extractorsUrl(connectionId))
    .catch(error => Promise.reject(error.response.data))
    .then(response => {
      const extractors = List(response.data.map(ExtractorFromJSON));

      store.updateState(state =>
        state.set(
          "byId",
          extractors.reduce(
            (byId, extractor) => byId.set(extractor.get("id", 0), extractor),
            state.get("byId")
          )
        )
      );

      return extractors;
    });

export const showExtractor = (connectionId: number, extractorId: number) => {
  const extractor = store
    .getState()
    .get("byId")
    .get(extractorId);

  if (extractor) {
    return Promise.resolve(extractor);
  } else {
    return Axios.get<IExtractorJSON>(
      extractorByIdUrl(connectionId, extractorId)
    )
      .catch(error => Promise.reject(error.response.data))
      .then(response => {
        const extractor = ExtractorFromJSON(response.data);

        store.updateState(state =>
          state.set(
            "byId",
            state.get("byId").set(extractor.get("id", 0), extractor)
          )
        );

        return extractor;
      });
  }
};

export const createExtractor = (
  connectionId: number,
  extractor: Partial<IExtractorJSON>
) =>
  Axios.post<IExtractorJSON>(extractorsUrl(connectionId), extractor)
    .catch(error => Promise.reject(error.response.data))
    .then(response => {
      const extractor = ExtractorFromJSON(response.data);

      store.updateState(state =>
        state.set(
          "byId",
          state.get("byId").set(extractor.get("id", 0), extractor)
        )
      );

      return extractor;
    });

export const updateExtractor = (
  connectionId: number,
  extractorId: number,
  extractor: Partial<IExtractorJSON>
) =>
  Axios.patch<IExtractorJSON>(
    extractorByIdUrl(connectionId, extractorId),
    extractor
  )
    .catch(error => Promise.reject(error.response.data))
    .then(response => {
      const extractor = ExtractorFromJSON(response.data);

      store.updateState(state =>
        state.set(
          "byId",
          state.get("byId").set(extractor.get("id", 0), extractor)
        )
      );

      return extractor;
    });

export const createDebounceUpdateExtractor = (): ((
  callbackFn: (updateFn: typeof updateExtractor) => void
) => void) =>
  debounce(onDebounce => onDebounce(updateExtractor), UPDATE_DEBOUNCE_MS, {
    before: startSync,
    after: endSync
  });

export const deleteExtractor = (
  connectionId: number,
  extractorId: number
): Promise<Option<Record<IExtractor> | Record<IPostgresqlExtractor>>> => {
  const extractor = store
    .getState()
    .get("byId")
    .get(extractorId);

  if (extractor) {
    store.updateState(state =>
      state.set("byId", state.get("byId").delete(extractor.get("id", 0)))
    );

    return Axios.delete<IExtractorJSON>(
      extractorByIdUrl(connectionId, extractorId)
    )
      .catch(error => Promise.reject(error.response.data))
      .then(() => some(extractor))
      .catch(() => {
        store.updateState(state =>
          state.set(
            "byId",
            state.get("byId").set(extractor.get("id", 0), extractor)
          )
        );
        return none();
      });
  } else {
    return Promise.reject(none());
  }
};
