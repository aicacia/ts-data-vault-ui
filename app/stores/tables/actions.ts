import { none, Option, some } from "@aicacia/core";
import { debounce } from "@aicacia/debounce";
import { IJSON } from "@aicacia/json";
import Axios from "axios";
import { List, Map, Record } from "immutable";
import { state } from "../../lib/state";
import { UPDATE_DEBOUNCE_MS } from "../shared";
import { endSync, startSync } from "../sync";
import {
  ITable,
  ITableJSON,
  ITablesJSON,
  STORE_NAME,
  Table,
  tableByIdUrl,
  TableField,
  Tables,
  tablesUrl
} from "./definitions";

export const store = state.getStore(STORE_NAME);

export const TableFromJSON = (tableJSON: ITableJSON) =>
  Table({
    ...tableJSON,
    fields: List(tableJSON.fields.map(TableField))
  });

store.fromJSON = (json: IJSON) => {
  const tablesJSON: ITablesJSON = json as any;

  return Tables({
    byId: Object.values(tablesJSON.byId).reduce(
      (byId, tableJSON) => byId.set(tableJSON.id, TableFromJSON(tableJSON)),
      Map<number, Record<ITable>>()
    )
  });
};

export const allTables = () =>
  Axios.get<ITableJSON[]>(tablesUrl())
    .catch(error => Promise.reject(error.response.data))
    .then(response => {
      const tables = List(response.data.map(TableFromJSON));

      store.updateState(state =>
        state.set(
          "byId",
          tables.reduce(
            (byId, table) => byId.set(table.get("id"), table),
            state.get("byId")
          )
        )
      );

      return tables;
    });

export const showTable = (tableId: number) => {
  const table = store
    .getState()
    .get("byId")
    .get(tableId);

  if (table) {
    return Promise.resolve(table);
  } else {
    return Axios.get<ITableJSON>(tableByIdUrl(tableId))
      .catch(error => Promise.reject(error.response.data))
      .then(response => {
        const table = TableFromJSON(response.data);

        store.updateState(state =>
          state.set("byId", state.get("byId").set(table.get("id"), table))
        );

        return table;
      });
  }
};

export const createTable = (table: Partial<ITableJSON>) =>
  Axios.post<ITableJSON>(tablesUrl(), table)
    .catch(error => Promise.reject(error.response.data))
    .then(response => {
      const table = TableFromJSON(response.data);

      store.updateState(state =>
        state.set("byId", state.get("byId").set(table.get("id"), table))
      );

      return table;
    });

export const updateTable = (tableId: number, table: Partial<ITableJSON>) =>
  Axios.patch<ITableJSON>(tableByIdUrl(tableId), table)
    .catch(error => Promise.reject(error.response.data))
    .then(response => {
      const table = TableFromJSON(response.data);

      store.updateState(state =>
        state.set("byId", state.get("byId").set(table.get("id"), table))
      );

      return table;
    });

export const createDebounceUpdateTable = (): ((
  callbackFn: (updateFn: typeof updateTable) => void
) => void) =>
  debounce(onDebounce => onDebounce(updateTable), UPDATE_DEBOUNCE_MS, {
    before: startSync,
    after: endSync
  });

export const deleteTable = (
  tableId: number
): Promise<Option<Record<ITable>>> => {
  const table = store
    .getState()
    .get("byId")
    .get(tableId);

  if (table) {
    store.updateState(state =>
      state.set("byId", state.get("byId").delete(table.get("id")))
    );

    return Axios.delete<ITableJSON>(tableByIdUrl(tableId))
      .catch(error => Promise.reject(error.response.data))
      .then(() => some(table))
      .catch(() => {
        store.updateState(state =>
          state.set("byId", state.get("byId").set(table.get("id"), table))
        );
        return none();
      });
  } else {
    return Promise.reject(none());
  }
};
