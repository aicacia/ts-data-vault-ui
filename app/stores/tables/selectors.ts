import { Option } from "@aicacia/core";
import { IState } from "../../lib/state";
import { STORE_NAME } from "./definitions";

export const selectTables = (state: IState) =>
  state
    .get(STORE_NAME)
    .byId.valueSeq()
    .toList();

export const selectTable = (state: IState, table_id: number) =>
  Option.from(state.get(STORE_NAME).byId.get(table_id));
