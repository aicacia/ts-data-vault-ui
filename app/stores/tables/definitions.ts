import { List, Map, Record } from "immutable";

export interface ITable {
  id: number;
  table_id: number;
  name: string;
  fields: List<Record<ITableField>>;
  inserted_at: string;
  updated_at: string;
}

export const Table = Record<ITable>({
  id: 0,
  table_id: 0,
  name: "",
  fields: List(),
  inserted_at: new Date().toJSON(),
  updated_at: new Date().toJSON()
});

export interface ITableField {
  id: number;
  name: string;
  type: string;
  tracked: boolean;
  primary_key: boolean;
  inserted_at: string;
  updated_at: string;
}

export const TableField = Record<ITableField>({
  id: 0,
  name: "",
  type: "string",
  tracked: true,
  primary_key: false,
  inserted_at: new Date().toJSON(),
  updated_at: new Date().toJSON()
});

export interface ITables {
  byId: Map<number, Record<ITable>>;
}

export const Tables = Record<ITables>({
  byId: Map()
});

export type ITableFieldJSON = ReturnType<Record<ITableField>["toJS"]>;
export interface ITableJSON extends ReturnType<Record<ITable>["toJS"]> {
  fields: ITableFieldJSON[];
}
export interface ITablesJSON {
  byId: { [id: number]: ITableJSON };
}

export const TABLES = "tables";
export const TABLES_URL = `/${TABLES}`;
export const TABLE_ID = "table_id";
export const TABLES_BY_ID_URL = `${TABLES_URL}/${TABLE_ID}`;

export const tablesUrl = () => TABLES_URL;
export const tableByIdUrl = (tableId: number) => `${tablesUrl}/${tableId}`;

export const INITIAL_STATE = Tables();
export const STORE_NAME = "tables";
