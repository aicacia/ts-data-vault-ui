import { Option } from "@aicacia/core";
import { IState } from "../../lib/state";
import { STORE_NAME } from "./definitions";

export const selectCronJobs = (state: IState) =>
  state
    .get(STORE_NAME)
    .byId.valueSeq()
    .toList();

export const selectCronJob = (state: IState, cron_job_id: number) =>
  Option.from(state.get(STORE_NAME).byId.get(cron_job_id));
