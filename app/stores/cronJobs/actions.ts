import { none, Option, some } from "@aicacia/core";
import { debounce } from "@aicacia/debounce";
import { IJSON } from "@aicacia/json";
import Axios from "axios";
import { List, Map, Record } from "immutable";
import { state } from "../../lib/state";
import { UPDATE_DEBOUNCE_MS } from "../shared";
import { endSync, startSync } from "../sync";
import {
  CronJob,
  cronJobByIdUrl,
  CronJobs,
  cronJobsUrl,
  ICronJob,
  ICronJobJSON,
  ICronJobsJSON,
  STORE_NAME
} from "./definitions";

export const store = state.getStore(STORE_NAME);

export const CronJobFromJSON = (cronJobJSON: ICronJobJSON) =>
  CronJob(cronJobJSON);

store.fromJSON = (json: IJSON) => {
  const cronJobsJSON: ICronJobsJSON = json as any;

  return CronJobs({
    byId: Object.values(cronJobsJSON.byId).reduce(
      (byId, cronJobJSON) =>
        byId.set(cronJobJSON.id, CronJobFromJSON(cronJobJSON)),
      Map<number, Record<ICronJob>>()
    )
  });
};

export const allCronJobs = () =>
  Axios.get<ICronJobJSON[]>(cronJobsUrl())
    .catch(error => Promise.reject(error.response.data))
    .then(response => {
      const cronJobs = List(response.data.map(CronJobFromJSON));

      store.updateState(state =>
        state.set(
          "byId",
          cronJobs.reduce(
            (byId, cronJob) => byId.set(cronJob.get("id"), cronJob),
            state.get("byId")
          )
        )
      );

      return cronJobs;
    });

export const showCronJob = (cronJobId: number) => {
  const cronJob = store
    .getState()
    .get("byId")
    .get(cronJobId);

  if (cronJob) {
    return Promise.resolve(cronJob);
  } else {
    return Axios.get<ICronJobJSON>(cronJobByIdUrl(cronJobId))
      .catch(error => Promise.reject(error.response.data))
      .then(response => {
        const cronJob = CronJobFromJSON(response.data);

        store.updateState(state =>
          state.set("byId", state.get("byId").set(cronJob.get("id"), cronJob))
        );

        return cronJob;
      });
  }
};

export const createCronJob = (cronJob: Partial<ICronJobJSON>) =>
  Axios.post<ICronJobJSON>(cronJobsUrl(), cronJob)
    .catch(error => Promise.reject(error.response.data))
    .then(response => {
      const cronJob = CronJobFromJSON(response.data);

      store.updateState(state =>
        state.set("byId", state.get("byId").set(cronJob.get("id"), cronJob))
      );

      return cronJob;
    });

export const updateCronJob = (
  cronJobId: number,
  cronJob: Partial<ICronJobJSON>
) =>
  Axios.patch<ICronJobJSON>(cronJobByIdUrl(cronJobId), cronJob)
    .catch(error => Promise.reject(error.response.data))
    .then(response => {
      const cronJob = CronJobFromJSON(response.data);

      store.updateState(state =>
        state.set("byId", state.get("byId").set(cronJob.get("id"), cronJob))
      );

      return cronJob;
    });

export const createDebounceUpdateCronJob = (): ((
  callbackFn: (updateFn: typeof updateCronJob) => void
) => void) =>
  debounce(onDebounce => onDebounce(updateCronJob), UPDATE_DEBOUNCE_MS, {
    before: startSync,
    after: endSync
  });

export const deleteCronJob = (
  cronJobId: number
): Promise<Option<Record<ICronJob>>> => {
  const cronJob = store
    .getState()
    .get("byId")
    .get(cronJobId);

  if (cronJob) {
    store.updateState(state =>
      state.set("byId", state.get("byId").delete(cronJob.get("id")))
    );

    return Axios.delete<ICronJobJSON>(cronJobByIdUrl(cronJobId))
      .catch(error => Promise.reject(error.response.data))
      .then(() => some(cronJob))
      .catch(() => {
        store.updateState(state =>
          state.set("byId", state.get("byId").set(cronJob.get("id"), cronJob))
        );
        return none();
      });
  } else {
    return Promise.reject(none());
  }
};
