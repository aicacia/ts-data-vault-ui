import { Map, Record } from "immutable";

export interface ICronJob {
  id: number;
  name: string;
  schedule: string;
  inserted_at: string;
  updated_at: string;
}

export const CronJob = Record<ICronJob>({
  id: 0,
  name: "",
  schedule: "0 0 * * *",
  inserted_at: new Date().toJSON(),
  updated_at: new Date().toJSON()
});

export interface ICronJobs {
  byId: Map<number, Record<ICronJob>>;
}

export const CronJobs = Record<ICronJobs>({
  byId: Map()
});

export type ICronJobJSON = ReturnType<Record<ICronJob>["toJS"]>;
export interface ICronJobsJSON {
  byId: { [id: number]: ICronJobJSON };
}

export const CRON_JOBS = "cron-jobs";
export const CRON_JOBS_URL = `/${CRON_JOBS}`;
export const CRON_JOB_ID = "cron_job_id";
export const CRON_JOBS_BY_ID_URL = `${CRON_JOBS_URL}/${CRON_JOB_ID}`;

export const cronJobsUrl = () => CRON_JOBS_URL;
export const cronJobByIdUrl = (cronJobId: number) =>
  `${cronJobsUrl()}/${cronJobId}`;

export const INITIAL_STATE = CronJobs();
export const STORE_NAME = "cronJobs";
