import { Record } from "immutable";

export interface ISidebar {
  open: boolean;
}

export const Sidebar = Record<ISidebar>({
  open: true
});

export type ISidebarJSON = ReturnType<Record<ISidebar>["toJS"]>;

export const INITIAL_STATE = Sidebar();
export const STORE_NAME = "sidebar";
