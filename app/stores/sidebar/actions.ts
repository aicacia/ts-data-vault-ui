import { state } from "../../lib/state";
import { ISidebarJSON, Sidebar, STORE_NAME } from "./definitions";

export const store = state.getStore(STORE_NAME);

store.fromJSON = (json: ISidebarJSON) =>
  Sidebar({
    open: json.open
  });

export const toggleSidebar = () =>
  store.updateState(state => state.update("open", open => !open));
