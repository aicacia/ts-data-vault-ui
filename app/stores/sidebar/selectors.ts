import { IState } from "../../lib/state";
import { STORE_NAME } from "./definitions";

export const isSidebarOpen = (state: IState) =>
  state.get(STORE_NAME).get("open");
