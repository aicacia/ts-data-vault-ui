import { none, Option, some } from "@aicacia/core";
import { debounce } from "@aicacia/debounce";
import { IJSON } from "@aicacia/json";
import Axios from "axios";
import { List, Map, Record } from "immutable";
import { state } from "../../lib/state";
import { UPDATE_DEBOUNCE_MS } from "../shared";
import { endSync, startSync } from "../sync";
import {
  connectionByIdUrl,
  Connections,
  connectionsUrl,
  IConnection,
  IConnectionJSON,
  IConnectionsJSON,
  isPostgresqlConnectionJSON,
  PostgresqlConnection,
  STORE_NAME
} from "./definitions";

export const store = state.getStore(STORE_NAME);

export const ConnectionFromJSON = (connectionJSON: IConnectionJSON) => {
  if (isPostgresqlConnectionJSON(connectionJSON)) {
    return PostgresqlConnection(connectionJSON);
  } else {
    throw new TypeError(
      `Invalid connection type ${JSON.stringify(connectionJSON)}`
    );
  }
};

store.fromJSON = (json: IJSON) => {
  const connectionsJSON: IConnectionsJSON = json as any;

  return Connections({
    byId: Object.values(connectionsJSON.byId).reduce(
      (byId, connectionJSON) =>
        byId.set(connectionJSON.id, ConnectionFromJSON(connectionJSON)),
      Map<number, Record<IConnection>>()
    )
  });
};

export const allConnections = () =>
  Axios.get<IConnectionJSON[]>(connectionsUrl())
    .catch(error => Promise.reject(error.response.data))
    .then(response => {
      const connections = List(response.data.map(ConnectionFromJSON));

      store.updateState(state =>
        state.set(
          "byId",
          connections.reduce(
            (byId, connection) => byId.set(connection.get("id", 0), connection),
            state.get("byId")
          )
        )
      );

      return connections;
    });

export const showConnection = (connectionId: number) => {
  const connection = store
    .getState()
    .get("byId")
    .get(connectionId);

  if (connection) {
    return Promise.resolve(connection);
  } else {
    return Axios.get<IConnectionJSON>(connectionByIdUrl(connectionId))
      .catch(error => Promise.reject(error.response.data))
      .then(response => {
        const connection = ConnectionFromJSON(response.data);

        store.updateState(state =>
          state.set(
            "byId",
            state.get("byId").set(connection.get("id", 0), connection)
          )
        );

        return connection;
      });
  }
};

export const createConnection = (connection: Partial<IConnectionJSON>) =>
  Axios.post<IConnectionJSON>(connectionsUrl(), connection)
    .catch(error => Promise.reject(error.response.data))
    .then(response => {
      const connection = ConnectionFromJSON(response.data);

      store.updateState(state =>
        state.set(
          "byId",
          state.get("byId").set(connection.get("id", 0), connection)
        )
      );

      return connection;
    });

export const updateConnection = (
  connectionId: number,
  connection: Partial<IConnectionJSON>
) =>
  Axios.patch<IConnectionJSON>(connectionByIdUrl(connectionId), connection)
    .catch(error => Promise.reject(error.response.data))
    .then(response => {
      const connection = ConnectionFromJSON(response.data);

      store.updateState(state =>
        state.set(
          "byId",
          state.get("byId").set(connection.get("id", 0), connection)
        )
      );

      return connection;
    });

export const createDebounceUpdateConnection = (): ((
  callbackFn: (updateFn: typeof updateConnection) => void
) => void) =>
  debounce(onDebounce => onDebounce(updateConnection), UPDATE_DEBOUNCE_MS, {
    before: startSync,
    after: endSync
  });

export const deleteConnection = (
  connectionId: number
): Promise<Option<Record<IConnection>>> => {
  const connection = store
    .getState()
    .get("byId")
    .get(connectionId);

  if (connection) {
    store.updateState(state =>
      state.set("byId", state.get("byId").delete(connection.get("id", 0)))
    );

    return Axios.delete<IConnectionJSON>(connectionByIdUrl(connectionId))
      .catch(error => Promise.reject(error.response.data))
      .then(() => some(connection))
      .catch(() => {
        store.updateState(state =>
          state.set(
            "byId",
            state.get("byId").set(connection.get("id", 0), connection)
          )
        );
        return none();
      });
  } else {
    return Promise.reject(none());
  }
};
