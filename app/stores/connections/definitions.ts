import { Map, Record } from "immutable";
import { ConnectionType } from "../shared";

export interface IBaseConnection {
  id: number;
  name: string;
  type: ConnectionType;
  inserted_at: string;
  updated_at: string;
}

export interface IPostgresqlConnection extends IBaseConnection {
  username: string;
  password: string;
  uri: string;
}

export const PostgresqlConnection = Record<IPostgresqlConnection>({
  id: 0,
  name: "",
  type: ConnectionType.POSTGRESQL,
  username: "",
  password: "",
  uri: "",
  inserted_at: new Date().toJSON(),
  updated_at: new Date().toJSON()
});

export type IConnection = IPostgresqlConnection;

export interface IConnections {
  byId: Map<number, Record<IConnection>>;
}

export const Connections = Record<IConnections>({
  byId: Map()
});

export type IPostgresqlConnectionJSON = ReturnType<
  Record<IPostgresqlConnection>["toJS"]
>;
export type IConnectionJSON = ReturnType<Record<IConnection>["toJS"]>;
export interface IConnectionsJSON {
  byId: { [id: number]: IConnectionJSON };
}

export function isPostgresqlConnection(
  connection: Record<IConnection>
): connection is Record<IPostgresqlConnectionJSON> {
  return connection.get("type") === ConnectionType.POSTGRESQL;
}

export function isPostgresqlConnectionJSON(
  connection: IConnectionJSON
): connection is IPostgresqlConnectionJSON {
  return connection.type === ConnectionType.POSTGRESQL;
}

export const CONNECTIONS = "connections";
export const CONNECTIONS_URL = `/${CONNECTIONS}`;
export const CONNECTION_ID = "connection_id";
export const CONNECTIONS_BY_ID_URL = `${CONNECTIONS_URL}/${CONNECTION_ID}`;

export const connectionsUrl = () => CONNECTIONS_URL;
export const connectionByIdUrl = (connectionId: number) =>
  `${connectionsUrl()}/${connectionId}`;

export const INITIAL_STATE = Connections();
export const STORE_NAME = "connections";
