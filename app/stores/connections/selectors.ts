import { Option } from "@aicacia/core";
import { IState } from "../../lib/state";
import { STORE_NAME } from "./definitions";

export const selectConnections = (state: IState) =>
  state
    .get(STORE_NAME)
    .byId.valueSeq()
    .toList();

export const selectConnection = (state: IState, connection_id: number) =>
  Option.from(state.get(STORE_NAME).byId.get(connection_id));
