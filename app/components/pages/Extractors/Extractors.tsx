import Octicon, { Plus } from "@primer/octicons-react";
import { none, Option, some } from "@aicacia/core";
import { List, Record } from "immutable";
import { default as React } from "react";
import { Button, Col, Container, Row } from "reactstrap";
import { connect } from "../../../lib/state";
import { CONNECTION_ID } from "../../../stores/connections";
import {
  allExtractors,
  IExtractor,
  selectExtractors
} from "../../../stores/extractors";
import { selectParam } from "../../../stores/lib/router";
import { DeleteExtractor } from "../../shared/DeleteExtractor";
import { EditExtractor } from "../../shared/EditExtractor";
import { Layout } from "../../shared/Layout";
import { Extractor } from "./Extractor/Extractor";

export interface IExtractorsStateProps {
  connectionId: number;
  extractors: List<Record<IExtractor>>;
}
export interface IExtractorsFunctionProps {}
export interface IExtractorsImplProps
  extends IExtractorsProps,
    IExtractorsStateProps,
    IExtractorsFunctionProps {}

export interface IExtractorsProps {}
export interface IExtractorsState {
  createConnection: boolean;
  deleteExtractor: Option<Record<IExtractor>>;
  editExtractor: Option<Record<IExtractor>>;
}

const ExtractorsConnect = connect<
  IExtractorsStateProps,
  IExtractorsFunctionProps,
  IExtractorsProps
>(
  (state, ownProps) => {
    const connectionId = selectParam(state, CONNECTION_ID) as number;
    return {
      connectionId,
      extractors: selectExtractors(state, connectionId)
    };
  },
  (state, ownProps, stateProps) => ({})
);

class ExtractorsImpl extends React.PureComponent<
  IExtractorsImplProps,
  IExtractorsState
> {
  state: IExtractorsState = {
    createConnection: false,
    deleteExtractor: none(),
    editExtractor: none()
  };
  componentDidMount() {
    allExtractors(this.props.connectionId);
  }
  onCreateOpen = () => this.setState({ createConnection: true });
  onCreateClose = () => this.setState({ createConnection: false });
  onEditOpen = (extractor: Record<IExtractor>) =>
    this.setState({ editExtractor: some(extractor) });
  onEditClose = () => this.setState({ editExtractor: none() });
  onDeleteOpen = (extractor: Record<IExtractor>) =>
    this.setState({ deleteExtractor: some(extractor) });
  onDeleteClose = () => this.setState({ deleteExtractor: none() });
  render() {
    return (
      <Layout>
        <Container>
          <Row>
            <Col>
              <Button
                className="float-right"
                color="success"
                onClick={this.onCreateOpen}
              >
                <Octicon icon={Plus} />
              </Button>
            </Col>
          </Row>
          <hr />
          <Row>
            {this.props.extractors.map((extractor, index) => (
              <Col key={extractor.get("id", index)}>
                <Extractor
                  extractor={extractor}
                  onEdit={this.onEditOpen}
                  onDelete={this.onDeleteOpen}
                />
              </Col>
            ))}
          </Row>
          <EditExtractor
            extractor={this.state.editExtractor}
            onClose={this.onEditClose}
          />
          <DeleteExtractor
            extractor={this.state.deleteExtractor}
            onClose={this.onDeleteClose}
          />
        </Container>
      </Layout>
    );
  }
}

export const Extractors = ExtractorsConnect(ExtractorsImpl);
