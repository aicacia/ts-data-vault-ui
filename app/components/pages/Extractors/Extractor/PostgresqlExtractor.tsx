import { Record } from "immutable";
import { default as React } from "react";
import { FormGroup, Input } from "reactstrap";
import { IPostgresqlExtractor } from "../../../../stores/extractors";

export interface IPostgresqlExtractorProps {
  extractor: Record<IPostgresqlExtractor>;
  onEdit(): void;
}

export interface IPostgresqlExtractorState {
  showPassword: boolean;
}

export class PostgresqlExtractor extends React.PureComponent<
  IPostgresqlExtractorProps,
  IPostgresqlExtractorState
> {
  state: IPostgresqlExtractorState = {
    showPassword: false
  };
  toggleShowPassword = () =>
    this.setState({ showPassword: !this.state.showPassword });
  render() {
    return (
      <>
        <FormGroup>
          <Input
            disabled
            type="textarea"
            value={this.props.extractor.get("query")}
          />
        </FormGroup>
      </>
    );
  }
}
