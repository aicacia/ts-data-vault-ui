import { Record } from "immutable";
import { default as React } from "react";
import { Card, CardBody, CardHeader } from "reactstrap";
import {
  IExtractor,
  isPostgresqlExtractor
} from "../../../../stores/extractors";
import { Header } from "./Header";
import { PostgresqlExtractor } from "./PostgresqlExtractor";

export interface IExtractorProps {
  extractor: Record<IExtractor>;
  onEdit(extractor: Record<IExtractor>): void;
  onDelete(extractor: Record<IExtractor>): void;
}

export class Extractor extends React.PureComponent<IExtractorProps> {
  onEdit = () => {
    this.props.onEdit(this.props.extractor);
  };
  onDelete = () => {
    this.props.onDelete(this.props.extractor);
  };
  render() {
    return (
      <Card>
        <CardHeader>
          <Header
            extractor={this.props.extractor}
            onEdit={this.onEdit}
            onDelete={this.onDelete}
          />
        </CardHeader>
        <CardBody>
          {isPostgresqlExtractor(this.props.extractor) ? (
            <PostgresqlExtractor
              extractor={this.props.extractor}
              onEdit={this.onEdit}
            />
          ) : null}
        </CardBody>
      </Card>
    );
  }
}
