import Octicon, { KebabVertical } from "@primer/octicons-react";
import { Record } from "immutable";
import { default as React } from "react";
import { FormattedMessage } from "react-intl";
import {
  Button,
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle
} from "reactstrap";
import { router } from "../../../../lib";
import {
  IExtractor,
  isPostgresqlExtractor
} from "../../../../stores/extractors";
import postgresql_image from "../../../shared/images/postgresql.png";

export interface IHeaderProps {
  extractor: Record<IExtractor>;
  onEdit(): void;
  onDelete(): void;
}
export interface IHeaderState {
  isDropdownOpen: boolean;
}

export class Header extends React.PureComponent<IHeaderProps, IHeaderState> {
  state: IHeaderState = {
    isDropdownOpen: false
  };
  toggleDropdown = () =>
    this.setState({ isDropdownOpen: !this.state.isDropdownOpen });
  render() {
    return (
      <div className="d-flex">
        <img
          className="mr-2"
          src={
            isPostgresqlExtractor(this.props.extractor) ? postgresql_image : ""
          }
          style={{ width: "32px", height: "32px" }}
        />
        {this.props.extractor.get("name")}
        <Dropdown
          className="ml-auto"
          isOpen={this.state.isDropdownOpen}
          toggle={this.toggleDropdown}
        >
          <DropdownToggle tag={Button}>
            <Octicon icon={KebabVertical} />
          </DropdownToggle>
          <DropdownMenu>
            <DropdownItem
              href={router.paths.Mappers(
                this.props.extractor.get("connection_id"),
                this.props.extractor.get("id")
              )}
            >
              <FormattedMessage id="extractors.view" />
            </DropdownItem>
            <DropdownItem onClick={this.props.onEdit}>
              <FormattedMessage id="extractors.edit" />
            </DropdownItem>
            <DropdownItem onClick={this.props.onDelete}>
              <FormattedMessage id="extractors.delete" />
            </DropdownItem>
          </DropdownMenu>
        </Dropdown>
      </div>
    );
  }
}
