import { default as React } from "react";
import { Container } from "reactstrap";
import { connect } from "../../../lib/state";
import { ISupporttedLocale, selectLocale } from "../../../stores/lib/locales";
import { SafeStatePureComponent } from "../../lib/SafeStatePureComponent";
import { Layout } from "../../shared/Layout";

const MARKDOWN = {
  en: import("./en.md"),
  de: import("./de.md")
};

interface ITermsStateProps {
  locale: ISupporttedLocale;
}
interface ITermsFunctionProps {}
interface ITermsImplProps extends ITermsStateProps, ITermsFunctionProps {}

export interface ITermsProps {}
export interface ITermsState {
  terms: string;
}

const TermsConnect = connect<
  ITermsStateProps,
  ITermsFunctionProps,
  ITermsProps
>(
  state => ({
    locale: selectLocale(state)
  }),
  (state, ownProps, stateProps) => ({})
);

class TermsImpl extends SafeStatePureComponent<ITermsImplProps, ITermsState> {
  constructor(props: ITermsImplProps) {
    super(props);

    this.state = {
      terms: ""
    };
  }
  componentDidMount() {
    super.componentDidMount();
    this.renderMarkdown();
  }
  componentDidUpdate(prev: ITermsImplProps) {
    this.renderMarkdown(prev.locale);
  }
  renderMarkdown(prevLocale?: ISupporttedLocale) {
    if (prevLocale !== this.props.locale) {
      MARKDOWN[this.props.locale].then((terms: any) =>
        this.safeSetState({ terms })
      );
    }
  }
  render() {
    return (
      <Layout>
        <Container dangerouslySetInnerHTML={{ __html: this.state.terms }} />
      </Layout>
    );
  }
}

export const Terms = TermsConnect(TermsImpl);
