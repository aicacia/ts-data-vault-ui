import { none, Option, some } from "@aicacia/core";
import Octicon, { Plus } from "@primer/octicons-react";
import { List, Record } from "immutable";
import { default as React } from "react";
import { Button, Col, Container, Row } from "reactstrap";
import { connect } from "../../../lib/state";
import { CONNECTION_ID } from "../../../stores/connections";
import { EXTRACTOR_ID } from "../../../stores/extractors";
import { selectParam } from "../../../stores/lib/router";
import { allMappers, IMapper, selectMappers } from "../../../stores/mappers";
import { allTables, ITable, selectTables } from "../../../stores/tables";
import { DeleteMapper } from "../../shared/DeleteMapper";
import { EditMapper } from "../../shared/EditMapper";
import { Layout } from "../../shared/Layout";
import { Mapper } from "./Mapper/Mapper";

export interface IMappersStateProps {
  connectionId: number;
  extractorId: number;
  tables: List<Record<ITable>>;
  mappers: List<Record<IMapper>>;
}
export interface IMappersFunctionProps {}
export interface IMappersImplProps
  extends IMappersProps,
    IMappersStateProps,
    IMappersFunctionProps {}

export interface IMappersProps {}
export interface IMappersState {
  createConnection: boolean;
  deleteMapper: Option<Record<IMapper>>;
  editMapper: Option<Record<IMapper>>;
}

const MappersConnect = connect<
  IMappersStateProps,
  IMappersFunctionProps,
  IMappersProps
>(
  (state, ownProps) => {
    const connectionId = selectParam(state, CONNECTION_ID) as number,
      extractorId = selectParam(state, EXTRACTOR_ID) as number;

    return {
      connectionId,
      extractorId,
      tables: selectTables(state),
      mappers: selectMappers(state, extractorId)
    };
  },
  (state, ownProps, stateProps) => ({})
);

class MappersImpl extends React.PureComponent<
  IMappersImplProps,
  IMappersState
> {
  state: IMappersState = {
    createConnection: false,
    deleteMapper: none(),
    editMapper: none()
  };
  componentDidMount() {
    allTables().then(() =>
      allMappers(this.props.connectionId, this.props.extractorId)
    );
  }
  onCreateOpen = () => this.setState({ createConnection: true });
  onCreateClose = () => this.setState({ createConnection: false });
  onEditOpen = (mapper: Record<IMapper>) =>
    this.setState({ editMapper: some(mapper) });
  onEditClose = () => this.setState({ editMapper: none() });
  onDeleteOpen = (mapper: Record<IMapper>) =>
    this.setState({ deleteMapper: some(mapper) });
  onDeleteClose = () => this.setState({ deleteMapper: none() });
  render() {
    return (
      <Layout>
        <Container>
          <Row>
            <Col>
              <Button
                className="float-right"
                color="success"
                onClick={this.onCreateOpen}
              >
                <Octicon icon={Plus} />
              </Button>
            </Col>
          </Row>
          <hr />
          <Row>
            {this.props.mappers.map((mapper, index) => (
              <Col key={mapper.get("id", index)}>
                <Mapper
                  mapper={mapper}
                  onEdit={this.onEditOpen}
                  onDelete={this.onDeleteOpen}
                />
              </Col>
            ))}
          </Row>
          <EditMapper
            mapper={this.state.editMapper}
            tables={this.props.tables}
            connectionId={this.props.connectionId}
            onClose={this.onEditClose}
          />
          <DeleteMapper
            mapper={this.state.deleteMapper}
            connectionId={this.props.connectionId}
            onClose={this.onDeleteClose}
          />
        </Container>
      </Layout>
    );
  }
}

export const Mappers = MappersConnect(MappersImpl);
