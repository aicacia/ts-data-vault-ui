import { Map, Record } from "immutable";
import { default as React } from "react";
import { FormattedMessage } from "react-intl";
import { Card, CardBody, CardHeader, Table as RSTable } from "reactstrap";
import { connect } from "../../../../lib";
import { IMapper } from "../../../../stores/mappers";
import { ITable, ITableField, selectTable } from "../../../../stores/tables";
import { Header } from "./Header";

export interface IMapperProps {
  mapper: Record<IMapper>;
  onEdit(mapper: Record<IMapper>): void;
  onDelete(mapper: Record<IMapper>): void;
}
export interface IMapperStateProps {
  table: Record<ITable>;
  tableFields: Map<number, Record<ITableField>>;
}
export interface IMapperFunctionProps {}
export interface IMapperImplProps
  extends IMapperProps,
    IMapperStateProps,
    IMapperFunctionProps {}

const MapperConnect = connect<
  IMapperStateProps,
  IMapperFunctionProps,
  IMapperProps
>(
  (state, ownProps) => {
    const table = selectTable(state, ownProps.mapper.get("table_id")).unwrap();

    return {
      table,
      tableFields: table
        .get("fields")
        .reduce(
          (tableFields, field) => tableFields.set(field.get("id"), field),
          Map()
        )
    };
  },
  (state, ownProps, stateProps) => ({})
);

class MapperImpl extends React.PureComponent<IMapperImplProps> {
  onEdit = () => {
    this.props.onEdit(this.props.mapper);
  };
  onDelete = () => {
    this.props.onDelete(this.props.mapper);
  };
  render() {
    return (
      <Card>
        <CardHeader>
          <Header
            mapper={this.props.mapper}
            onEdit={this.onEdit}
            onDelete={this.onDelete}
          />
        </CardHeader>
        <CardBody>
          <RSTable striped borderless>
            <thead>
              <tr>
                <th>
                  <FormattedMessage id="mappers.name" />
                </th>
                <th>
                  <FormattedMessage id="mappers.tableFieldId" />
                </th>
              </tr>
            </thead>
            <tbody>
              {this.props.mapper.get("fields").map(field => (
                <tr key={field.get("id")}>
                  <th>{field.get("from")}</th>
                  <th>
                    {this.props.tableFields
                      .get(field.get("to_id"))
                      ?.get("name")}
                  </th>
                </tr>
              ))}
            </tbody>
          </RSTable>
        </CardBody>
      </Card>
    );
  }
}

export const Mapper = MapperConnect(MapperImpl);
