import Octicon, { KebabVertical } from "@primer/octicons-react";
import { Record } from "immutable";
import { default as React } from "react";
import { FormattedMessage } from "react-intl";
import {
  Button,
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle
} from "reactstrap";
import { router } from "../../../../lib";
import {
  IConnection,
  isPostgresqlConnection
} from "../../../../stores/connections";
import postgresql_image from "../../../shared/images/postgresql.png";

export interface IHeaderProps {
  connection: Record<IConnection>;
  onEdit(): void;
  onDelete(): void;
}
export interface IHeaderState {
  isDropdownOpen: boolean;
}

export class Header extends React.PureComponent<IHeaderProps, IHeaderState> {
  state: IHeaderState = {
    isDropdownOpen: false
  };
  toggleDropdown = () =>
    this.setState({ isDropdownOpen: !this.state.isDropdownOpen });
  render() {
    return (
      <div className="d-flex">
        <img
          className="mr-2"
          src={
            isPostgresqlConnection(this.props.connection)
              ? postgresql_image
              : ""
          }
          style={{ width: "32px", height: "32px" }}
        />
        {this.props.connection.get("name")}
        <Dropdown
          className="ml-auto"
          isOpen={this.state.isDropdownOpen}
          toggle={this.toggleDropdown}
        >
          <DropdownToggle tag={Button}>
            <Octicon icon={KebabVertical} />
          </DropdownToggle>
          <DropdownMenu>
            <DropdownItem
              href={router.paths.Extractors(this.props.connection.get("id"))}
            >
              <FormattedMessage id="connections.view" />
            </DropdownItem>
            <DropdownItem onClick={this.props.onEdit}>
              <FormattedMessage id="connections.edit" />
            </DropdownItem>
            <DropdownItem onClick={this.props.onDelete}>
              <FormattedMessage id="connections.delete" />
            </DropdownItem>
          </DropdownMenu>
        </Dropdown>
      </div>
    );
  }
}
