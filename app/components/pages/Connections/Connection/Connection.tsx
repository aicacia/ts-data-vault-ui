import { Record } from "immutable";
import { default as React } from "react";
import { Card, CardBody, CardHeader } from "reactstrap";
import {
  IConnection,
  isPostgresqlConnection
} from "../../../../stores/connections";
import { Header } from "./Header";
import { PostgresqlConnection } from "./PostgresqlConnection";

export interface IConnectionProps {
  connection: Record<IConnection>;
  onEdit(connection: Record<IConnection>): void;
  onDelete(connection: Record<IConnection>): void;
}

export class Connection extends React.PureComponent<IConnectionProps> {
  onEdit = () => {
    this.props.onEdit(this.props.connection);
  };
  onDelete = () => {
    this.props.onDelete(this.props.connection);
  };
  render() {
    return (
      <Card>
        <CardHeader>
          <Header
            connection={this.props.connection}
            onEdit={this.onEdit}
            onDelete={this.onDelete}
          />
        </CardHeader>
        <CardBody>
          {isPostgresqlConnection(this.props.connection) ? (
            <PostgresqlConnection
              connection={this.props.connection}
              onEdit={this.onEdit}
            />
          ) : null}
        </CardBody>
      </Card>
    );
  }
}
