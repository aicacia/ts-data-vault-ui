import Octicon, { Eye, EyeClosed } from "@primer/octicons-react";
import { Record } from "immutable";
import { default as React } from "react";
import { Button, FormGroup, Input, InputGroup } from "reactstrap";
import { IPostgresqlConnection } from "../../../../stores/connections";

export interface IPostgresqlConnectionProps {
  connection: Record<IPostgresqlConnection>;
  onEdit(): void;
}

export interface IPostgresqlConnectionState {
  showPassword: boolean;
}

export class PostgresqlConnection extends React.PureComponent<
  IPostgresqlConnectionProps,
  IPostgresqlConnectionState
> {
  state: IPostgresqlConnectionState = {
    showPassword: false
  };
  toggleShowPassword = () =>
    this.setState({ showPassword: !this.state.showPassword });
  render() {
    return (
      <>
        <FormGroup>
          <Input disabled value={this.props.connection.get("uri")} />
        </FormGroup>
        <FormGroup>
          <Input disabled value={this.props.connection.get("username")} />
        </FormGroup>
        <FormGroup>
          <InputGroup>
            <Input
              type={this.state.showPassword ? "text" : "password"}
              disabled
              value={this.props.connection.get("password")}
            />
            <Button onClick={this.toggleShowPassword}>
              {this.state.showPassword ? (
                <Octicon icon={EyeClosed} />
              ) : (
                <Octicon icon={Eye} />
              )}
            </Button>
          </InputGroup>
        </FormGroup>
      </>
    );
  }
}
