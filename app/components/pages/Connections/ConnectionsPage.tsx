import { default as React } from "react";
import { Helmet } from "react-helmet";
import { injectIntl } from "react-intl";
import { Async } from "../../lib/Async";
import { JSError } from "../../lib/JSError";
import { Loading } from "../../lib/Loading";
import { Page } from "../../lib/Page";

export const ConnectionsPage = injectIntl(({ intl }) => (
  <Async
    promise={import("./Connections")}
    onSuccess={({ Connections }) => (
      <Page>
        <Helmet>
          <title>{intl.formatMessage({ id: "app.page.connections" })}</title>
        </Helmet>
        <Connections />
      </Page>
    )}
    onError={error => <JSError error={error} />}
    onPending={() => <Loading />}
  />
));
