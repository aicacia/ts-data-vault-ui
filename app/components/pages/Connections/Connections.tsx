import Octicon, { Plus } from "@primer/octicons-react";
import { none, Option, some } from "@aicacia/core";
import { List, Record } from "immutable";
import { default as React } from "react";
import { Button, Col, Container, Row } from "reactstrap";
import { connect } from "../../../lib/state";
import {
  allConnections,
  IConnection,
  selectConnections
} from "../../../stores/connections";
import { DeleteConnection } from "../../shared/DeleteConnection";
import { EditConnection } from "../../shared/EditConnection";
import { Layout } from "../../shared/Layout";
import { Connection } from "./Connection/Connection";

export interface IConnectionsStateProps {
  connections: List<Record<IConnection>>;
}
export interface IConnectionsFunctionProps {}
export interface IConnectionsImplProps
  extends IConnectionsProps,
    IConnectionsStateProps,
    IConnectionsFunctionProps {}

export interface IConnectionsProps {}
export interface IConnectionsState {
  createConnection: boolean;
  deleteConnection: Option<Record<IConnection>>;
  editConnection: Option<Record<IConnection>>;
}

const ConnectionsConnect = connect<
  IConnectionsStateProps,
  IConnectionsFunctionProps,
  IConnectionsProps
>(
  (state, ownProps) => ({
    connections: selectConnections(state)
  }),
  (state, ownProps, stateProps) => ({})
);

class ConnectionsImpl extends React.PureComponent<
  IConnectionsImplProps,
  IConnectionsState
> {
  state: IConnectionsState = {
    createConnection: false,
    deleteConnection: none(),
    editConnection: none()
  };
  componentDidMount() {
    allConnections();
  }
  onCreateOpen = () => this.setState({ createConnection: true });
  onCreateClose = () => this.setState({ createConnection: false });
  onEditOpen = (connection: Record<IConnection>) =>
    this.setState({ editConnection: some(connection) });
  onEditClose = () => this.setState({ editConnection: none() });
  onDeleteOpen = (connection: Record<IConnection>) =>
    this.setState({ deleteConnection: some(connection) });
  onDeleteClose = () => this.setState({ deleteConnection: none() });
  render() {
    return (
      <Layout>
        <Container>
          <Row>
            <Col>
              <Button
                className="float-right"
                color="success"
                onClick={this.onCreateOpen}
              >
                <Octicon icon={Plus} />
              </Button>
            </Col>
          </Row>
          <hr />
          <Row>
            {this.props.connections.map((connection, index) => (
              <Col key={connection.get("id", index)}>
                <Connection
                  connection={connection}
                  onEdit={this.onEditOpen}
                  onDelete={this.onDeleteOpen}
                />
              </Col>
            ))}
          </Row>
          <EditConnection
            connection={this.state.editConnection}
            onClose={this.onEditClose}
          />
          <DeleteConnection
            connection={this.state.deleteConnection}
            onClose={this.onDeleteClose}
          />
        </Container>
      </Layout>
    );
  }
}

export const Connections = ConnectionsConnect(ConnectionsImpl);
