import Octicon, { Plus } from "@primer/octicons-react";
import { none, Option, some } from "@aicacia/core";
import { List, Record } from "immutable";
import { default as React } from "react";
import { Button, Col, Container, Row } from "reactstrap";
import { connect } from "../../../lib/state";
import {
  allCronJobs,
  ICronJob,
  selectCronJobs
} from "../../../stores/cronJobs";
import { DeleteCronJob } from "../../shared/DeleteCronJob";
import { EditCronJob } from "../../shared/EditCronJob";
import { Layout } from "../../shared/Layout";
import { CronJob } from "./CronJob";

export interface ICronJobsStateProps {
  cronJobs: List<Record<ICronJob>>;
}
export interface ICronJobsFunctionProps {}
export interface ICronJobsImplProps
  extends ICronJobsProps,
    ICronJobsStateProps,
    ICronJobsFunctionProps {}

export interface ICronJobsProps {}
export interface ICronJobsState {
  createCronJob: boolean;
  deleteCronJob: Option<Record<ICronJob>>;
  editCronJob: Option<Record<ICronJob>>;
}

const CronJobsConnect = connect<
  ICronJobsStateProps,
  ICronJobsFunctionProps,
  ICronJobsProps
>(
  (state, ownProps) => ({
    cronJobs: selectCronJobs(state)
  }),
  (state, ownProps, stateProps) => ({})
);

class CronJobsImpl extends React.PureComponent<
  ICronJobsImplProps,
  ICronJobsState
> {
  state: ICronJobsState = {
    createCronJob: false,
    deleteCronJob: none(),
    editCronJob: none()
  };
  componentDidMount() {
    allCronJobs();
  }
  onCreateOpen = () => this.setState({ createCronJob: true });
  onCreateClose = () => this.setState({ createCronJob: false });
  onEditOpen = (cronJob: Record<ICronJob>) =>
    this.setState({ editCronJob: some(cronJob) });
  onEditClose = () => this.setState({ editCronJob: none() });
  onDeleteOpen = (cronJob: Record<ICronJob>) =>
    this.setState({ deleteCronJob: some(cronJob) });
  onDeleteClose = () => this.setState({ deleteCronJob: none() });
  render() {
    return (
      <Layout>
        <Container>
          <Row>
            <Col>
              <Button
                className="float-right"
                color="success"
                onClick={this.onCreateOpen}
              >
                <Octicon icon={Plus} />
              </Button>
            </Col>
          </Row>
          <hr />
          <Row>
            {this.props.cronJobs.map((cronJob, index) => (
              <Col md={4} key={cronJob.get("id", index)}>
                <CronJob
                  cronJob={cronJob}
                  onEdit={this.onEditOpen}
                  onDelete={this.onDeleteOpen}
                />
              </Col>
            ))}
          </Row>
          <EditCronJob
            cronJob={this.state.editCronJob}
            onClose={this.onEditClose}
          />
          <DeleteCronJob
            cronJob={this.state.deleteCronJob}
            onClose={this.onDeleteClose}
          />
        </Container>
      </Layout>
    );
  }
}

export const CronJobs = CronJobsConnect(CronJobsImpl);
