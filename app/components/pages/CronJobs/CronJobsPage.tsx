import { default as React } from "react";
import { Helmet } from "react-helmet";
import { injectIntl } from "react-intl";
import { Async } from "../../lib/Async";
import { Page } from "../../lib/Page";
import { JSError } from "../../lib/JSError";
import { Loading } from "../../lib/Loading";

export const CronJobsPage = injectIntl(({ intl }) => (
    <Async
        promise={import("./CronJobs")}
        onSuccess={({ CronJobs }) => (
            <Page>
                <Helmet>
                    <title>
                        {intl.formatMessage({ id: "app.page.cronJobs" })}
                    </title>
                </Helmet>
                <CronJobs />
            </Page>
        )}
        onError={error => <JSError error={error} />}
        onPending={() => <Loading />}
    />
));
