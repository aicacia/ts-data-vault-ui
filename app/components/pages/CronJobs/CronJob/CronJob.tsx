import { Record } from "immutable";
import { default as React } from "react";
import { Card, CardBody, CardHeader, FormGroup, Input } from "reactstrap";
import { ICronJob } from "../../../../stores/cronJobs";
import { Header } from "./Header";

export interface ICronJobProps {
  cronJob: Record<ICronJob>;
  onEdit(cronJob: Record<ICronJob>): void;
  onDelete(cronJob: Record<ICronJob>): void;
}

export class CronJob extends React.PureComponent<ICronJobProps> {
  onEdit = () => {
    this.props.onEdit(this.props.cronJob);
  };
  onDelete = () => {
    this.props.onDelete(this.props.cronJob);
  };
  render() {
    return (
      <Card>
        <CardHeader>
          <Header
            cronJob={this.props.cronJob}
            onEdit={this.onEdit}
            onDelete={this.onDelete}
          />
        </CardHeader>
        <CardBody>
          <FormGroup>
            <Input disabled value={this.props.cronJob.get("schedule")} />
          </FormGroup>
        </CardBody>
      </Card>
    );
  }
}
