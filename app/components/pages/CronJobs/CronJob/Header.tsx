import Octicon, { KebabVertical } from "@primer/octicons-react";
import { Record } from "immutable";
import { default as React } from "react";
import { FormattedMessage } from "react-intl";
import {
  Button,
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle
} from "reactstrap";
import { ICronJob } from "../../../../stores/cronJobs";

export interface IHeaderProps {
  cronJob: Record<ICronJob>;
  onEdit(): void;
  onDelete(): void;
}
export interface IHeaderState {
  isDropdownOpen: boolean;
}

export class Header extends React.PureComponent<IHeaderProps, IHeaderState> {
  state: IHeaderState = {
    isDropdownOpen: false
  };
  toggleDropdown = () =>
    this.setState({ isDropdownOpen: !this.state.isDropdownOpen });
  render() {
    return (
      <div className="d-flex">
        {this.props.cronJob.get("name")}
        <Dropdown
          className="ml-auto"
          isOpen={this.state.isDropdownOpen}
          toggle={this.toggleDropdown}
        >
          <DropdownToggle tag={Button}>
            <Octicon icon={KebabVertical} />
          </DropdownToggle>
          <DropdownMenu>
            <DropdownItem onClick={this.props.onEdit}>
              <FormattedMessage id="cronJobs.edit" />
            </DropdownItem>
            <DropdownItem onClick={this.props.onDelete}>
              <FormattedMessage id="cronJobs.delete" />
            </DropdownItem>
          </DropdownMenu>
        </Dropdown>
      </div>
    );
  }
}
