import { default as React } from "react";
import { Container } from "reactstrap";
import { connect } from "../../../lib/state";
import { ISupporttedLocale, selectLocale } from "../../../stores/lib/locales";
import { SafeStatePureComponent } from "../../lib/SafeStatePureComponent";
import { Layout } from "../../shared/Layout";

const MARKDOWN = {
  en: import("./en.md"),
  de: import("./de.md")
};

interface IPrivacyPolicyStateProps {
  locale: ISupporttedLocale;
}
interface IPrivacyPolicyFunctionProps {}
interface IPrivacyPolicyImplProps
  extends IPrivacyPolicyStateProps,
    IPrivacyPolicyFunctionProps {}

export interface IPrivacyPolicyProps {}
export interface IPrivacyPolicyState {
  policy: string;
}

const PrivacyPolicyConnect = connect<
  IPrivacyPolicyStateProps,
  IPrivacyPolicyFunctionProps,
  IPrivacyPolicyProps
>(
  (state, ownProps) => ({
    locale: selectLocale(state)
  }),
  (state, ownProps, stateProps) => ({})
);

class PrivacyPolicyImpl extends SafeStatePureComponent<
  IPrivacyPolicyImplProps,
  IPrivacyPolicyState
> {
  constructor(props: IPrivacyPolicyImplProps) {
    super(props);

    this.state = {
      policy: ""
    };
  }
  componentDidMount() {
    super.componentDidMount();
    this.renderMarkdown();
  }
  componentDidUpdate(prev: IPrivacyPolicyImplProps) {
    this.renderMarkdown(prev.locale);
  }
  renderMarkdown(prevLocale?: ISupporttedLocale) {
    if (prevLocale !== this.props.locale) {
      MARKDOWN[this.props.locale].then((policy: any) =>
        this.safeSetState({ policy })
      );
    }
  }
  render() {
    return (
      <Layout>
        <Container dangerouslySetInnerHTML={{ __html: this.state.policy }} />
      </Layout>
    );
  }
}

export const PrivacyPolicy = PrivacyPolicyConnect(PrivacyPolicyImpl);
