import { default as React } from "react";
import { Helmet } from "react-helmet";
import { injectIntl } from "react-intl";
import { Async } from "../../lib/Async";
import { JSError } from "../../lib/JSError";
import { Loading } from "../../lib/Loading";
import { Page } from "../../lib/Page";

export const Error500Page = injectIntl(({ intl }) => (
  <Async
    promise={import("./Error500")}
    onSuccess={({ Error500 }) => (
      <Page>
        <Helmet>
          <title>{intl.formatMessage({ id: "app.page.error500" })}</title>
        </Helmet>
        <Error500 />
      </Page>
    )}
    onError={error => <JSError error={error} />}
    onPending={() => <Loading />}
  />
));
