import { default as React } from "react";
import { Helmet } from "react-helmet";
import { injectIntl } from "react-intl";
import { Async } from "../../lib/Async";
import { JSError } from "../../lib/JSError";
import { Loading } from "../../lib/Loading";

export const IndexPage = injectIntl(({ intl }) => (
  <Async
    promise={import("./Index")}
    onSuccess={({ Index }) => (
      <div>
        <Helmet>
          <title>{intl.formatMessage({ id: "app.page.home" })}</title>
        </Helmet>
        <Index />
      </div>
    )}
    onError={error => <JSError error={error} />}
    onPending={() => <Loading />}
  />
));
