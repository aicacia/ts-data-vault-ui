import Octicon, { Check, X } from "@primer/octicons-react";
import { Record } from "immutable";
import { default as React } from "react";
import { FormattedMessage } from "react-intl";
import { Card, CardBody, CardHeader, Table as RSTable } from "reactstrap";
import { ITable } from "../../../../stores/tables";
import { Header } from "./Header";

export interface ITableProps {
  table: Record<ITable>;
  onEdit(table: Record<ITable>): void;
  onDelete(table: Record<ITable>): void;
}

export class Table extends React.PureComponent<ITableProps> {
  onEdit = () => {
    this.props.onEdit(this.props.table);
  };
  onDelete = () => {
    this.props.onDelete(this.props.table);
  };
  render() {
    return (
      <Card>
        <CardHeader>
          <Header
            table={this.props.table}
            onEdit={this.onEdit}
            onDelete={this.onDelete}
          />
        </CardHeader>
        <CardBody>
          <RSTable striped borderless>
            <thead>
              <tr>
                <th>
                  <FormattedMessage id="tables.name" />
                </th>
                <th>
                  <FormattedMessage id="tables.type" />
                </th>
                <th>
                  <FormattedMessage id="tables.tracked" />
                </th>
                <th>
                  <FormattedMessage id="tables.primaryKey" />
                </th>
              </tr>
            </thead>
            <tbody>
              {this.props.table.get("fields").map(field => (
                <tr key={field.get("id")}>
                  <th>{field.get("name")}</th>
                  <th>:{field.get("type")}</th>
                  <th>
                    <Octicon icon={field.get("tracked") ? Check : X} />
                  </th>
                  <th>
                    <Octicon icon={field.get("primary_key") ? Check : X} />
                  </th>
                </tr>
              ))}
            </tbody>
          </RSTable>
        </CardBody>
      </Card>
    );
  }
}
