import Octicon, { KebabVertical } from "@primer/octicons-react";
import { Record } from "immutable";
import { default as React } from "react";
import { FormattedMessage } from "react-intl";
import {
  Button,
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle
} from "reactstrap";
import { ITable } from "../../../../stores/tables";

export interface IHeaderProps {
  table: Record<ITable>;
  onEdit(): void;
  onDelete(): void;
}
export interface IHeaderState {
  isDropdownOpen: boolean;
}

export class Header extends React.PureComponent<IHeaderProps, IHeaderState> {
  state: IHeaderState = {
    isDropdownOpen: false
  };
  toggleDropdown = () =>
    this.setState({ isDropdownOpen: !this.state.isDropdownOpen });
  render() {
    return (
      <div className="d-flex">
        {this.props.table.get("name")}
        <Dropdown
          className="ml-auto"
          isOpen={this.state.isDropdownOpen}
          toggle={this.toggleDropdown}
        >
          <DropdownToggle tag={Button}>
            <Octicon icon={KebabVertical} />
          </DropdownToggle>
          <DropdownMenu>
            <DropdownItem>
              <FormattedMessage id="tables.view" />
            </DropdownItem>
            <DropdownItem onClick={this.props.onEdit}>
              <FormattedMessage id="tables.edit" />
            </DropdownItem>
            <DropdownItem onClick={this.props.onDelete}>
              <FormattedMessage id="tables.delete" />
            </DropdownItem>
          </DropdownMenu>
        </Dropdown>
      </div>
    );
  }
}
