import { default as React } from "react";
import { Helmet } from "react-helmet";
import { injectIntl } from "react-intl";
import { Async } from "../../lib/Async";
import { Page } from "../../lib/Page";
import { JSError } from "../../lib/JSError";
import { Loading } from "../../lib/Loading";

export const TablesPage = injectIntl(({ intl }) => (
    <Async
        promise={import("./Tables")}
        onSuccess={({ Tables }) => (
            <Page>
                <Helmet>
                    <title>
                        {intl.formatMessage({ id: "app.page.tables" })}
                    </title>
                </Helmet>
                <Tables />
            </Page>
        )}
        onError={error => <JSError error={error} />}
        onPending={() => <Loading />}
    />
));
