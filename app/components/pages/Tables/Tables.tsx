import Octicon, { Plus } from "@primer/octicons-react";
import { none, Option, some } from "@aicacia/core";
import { List, Record } from "immutable";
import { default as React } from "react";
import { Button, Col, Container, Row } from "reactstrap";
import { connect } from "../../../lib/state";
import { allTables, ITable, selectTables } from "../../../stores/tables";
import { DeleteTable } from "../../shared/DeleteTable";
import { EditTable } from "../../shared/EditTable";
import { Layout } from "../../shared/Layout";
import { Table } from "./Table";

export interface ITablesStateProps {
  tables: List<Record<ITable>>;
}
export interface ITablesFunctionProps {}
export interface ITablesImplProps
  extends ITablesProps,
    ITablesStateProps,
    ITablesFunctionProps {}

export interface ITablesProps {}
export interface ITablesState {
  createTable: boolean;
  deleteTable: Option<Record<ITable>>;
  editTable: Option<Record<ITable>>;
}

const TablesConnect = connect<
  ITablesStateProps,
  ITablesFunctionProps,
  ITablesProps
>(
  (state, ownProps) => ({
    tables: selectTables(state)
  }),
  (state, ownProps, stateProps) => ({})
);

class TablesImpl extends React.PureComponent<ITablesImplProps, ITablesState> {
  state: ITablesState = {
    createTable: false,
    deleteTable: none(),
    editTable: none()
  };
  componentDidMount() {
    allTables();
  }
  onCreateOpen = () => this.setState({ createTable: true });
  onCreateClose = () => this.setState({ createTable: false });
  onEditOpen = (table: Record<ITable>) =>
    this.setState({ editTable: some(table) });
  onEditClose = () => this.setState({ editTable: none() });
  onDeleteOpen = (table: Record<ITable>) =>
    this.setState({ deleteTable: some(table) });
  onDeleteClose = () => this.setState({ deleteTable: none() });
  render() {
    return (
      <Layout>
        <Container>
          <Row>
            <Col>
              <Button
                className="float-right"
                color="success"
                onClick={this.onCreateOpen}
              >
                <Octicon icon={Plus} />
              </Button>
            </Col>
          </Row>
          <hr />
          <Row>
            {this.props.tables.map((table, index) => (
              <Col key={table.get("id", index)}>
                <Table
                  table={table}
                  onEdit={this.onEditOpen}
                  onDelete={this.onDeleteOpen}
                />
              </Col>
            ))}
          </Row>
          <EditTable table={this.state.editTable} onClose={this.onEditClose} />
          <DeleteTable
            table={this.state.deleteTable}
            onClose={this.onDeleteClose}
          />
        </Container>
      </Layout>
    );
  }
}

export const Tables = TablesConnect(TablesImpl);
