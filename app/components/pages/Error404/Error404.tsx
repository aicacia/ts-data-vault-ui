import { default as React } from "react";
import { FormattedMessage } from "react-intl";
import { Container } from "reactstrap";
import { connect } from "../../../lib/state";
import { Layout } from "../../shared/Layout";

interface IError404StateProps {}
interface IError404FunctionProps {}
interface IError404ImplProps
  extends IError404StateProps,
    IError404FunctionProps {}

export interface IError404Props {}
export interface IError404State {}

const Error404Connect = connect<
  IError404StateProps,
  IError404FunctionProps,
  IError404ImplProps
>(
  state => ({}),
  (state, ownProps, stateProps) => ({})
);

class Error404Impl extends React.PureComponent<
  IError404ImplProps,
  IError404State
> {
  render() {
    return (
      <Layout>
        <Container>
          <h1>
            <FormattedMessage id="error.404.name" />
          </h1>
          <p>
            <FormattedMessage id="error.404.message" />
          </p>
        </Container>
      </Layout>
    );
  }
}

export const Error404 = Error404Connect(Error404Impl);
