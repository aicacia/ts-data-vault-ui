import { Option } from "@aicacia/core";
import { Record } from "immutable";
import { default as React } from "react";
import { FormattedMessage } from "react-intl";
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";
import { deleteExtractor, IExtractor } from "../../stores/extractors";

export interface IDeleteExtractorProps {
  extractor: Option<Record<IExtractor>>;
  onClose(): void;
}

export interface IDeleteExtractorState {
  loading: boolean;
}

export class DeleteExtractor extends React.PureComponent<
  IDeleteExtractorProps,
  IDeleteExtractorState
> {
  state: IDeleteExtractorState = {
    loading: false
  };
  onDelete = () => {
    this.props.extractor.map(extractor => {
      this.setState({ loading: true });
      deleteExtractor(extractor.get("connection_id"), extractor.get("id"))
        .then(this.props.onClose)
        .catch(() => {
          this.setState({ loading: false });
          this.props.onClose();
        });
    });
  };
  render() {
    return (
      <Modal isOpen={this.props.extractor.isSome()}>
        <ModalHeader toggle={this.props.onClose}>
          {this.props.extractor
            .map(extractor => extractor.get("name", ""))
            .unwrapOr("")}
        </ModalHeader>
        <ModalBody>
          <FormattedMessage
            id="extractors.delete_model.message"
            values={{
              name: this.props.extractor
                .map(extractor => extractor.get("name"))
                .unwrapOr("")
            }}
          />
        </ModalBody>
        <ModalFooter>
          <Button color="danger" onClick={this.onDelete}>
            <FormattedMessage id="extractors.delete_model.confirm" />
          </Button>
          <Button onClick={this.props.onClose}>
            <FormattedMessage id="extractors.delete_model.cancel" />
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}
