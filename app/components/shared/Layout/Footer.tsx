import { default as React } from "react";
import { FormattedMessage } from "react-intl";

export interface IHeaderProps {
  refObject?: React.RefObject<HTMLDivElement>;
}
export interface IHeaderState {}

export class Footer extends React.PureComponent<IHeaderProps, IHeaderState> {
  render() {
    return (
      <div
        ref={this.props.refObject}
        className="page-footer border-top pt-2 pb-2"
      >
        <p className="footer-copyright text-center m-0">
          <FormattedMessage
            id="app.footer.copyright"
            values={{
              year: new Date().getFullYear(),
              value: <a href="/">{window.location.host}</a>
            }}
          />
        </p>
      </div>
    );
  }
}
