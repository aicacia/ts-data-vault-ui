import Octicon, { ThreeBars } from "@primer/octicons-react";
import { default as React } from "react";
import { FormattedMessage } from "react-intl";
import { Button, Col, Nav, Navbar, NavItem, NavLink, Row } from "reactstrap";
import { router } from "../../../lib";
import { connect } from "../../../lib/state";
import { CONNECTIONS_URL } from "../../../stores/connections";
import { CRON_JOBS_URL } from "../../../stores/cronJobs";
import { selectHeight, selectWidth } from "../../../stores/lib/screenSize";
import { isSidebarOpen, toggleSidebar } from "../../../stores/sidebar";
import { isSyncing } from "../../../stores/sync";
import { TABLES_URL } from "../../../stores/tables";
import { Sidebar } from "../Sidebar";
import { Sizes } from "../Sizes";
import { Brand } from "./Brand";
import { Footer } from "./Footer";

interface IHeaderStateProps {
  width: number;
  height: number;
  isSyncing: boolean;
  sidebarOpen: boolean;
}
interface IHeaderFunctionProps {}

interface IHeaderImplProps extends IHeaderStateProps, IHeaderFunctionProps {}

export interface IHeaderProps {}
export interface IHeaderState {}

const HeaderConnect = connect<
  IHeaderStateProps,
  IHeaderFunctionProps,
  IHeaderProps
>(
  (state, ownProps) => ({
    width: selectWidth(state),
    height: selectHeight(state),
    isSyncing: isSyncing(state),
    sidebarOpen: isSidebarOpen(state)
  }),
  (state, ownProps, stateProps) => ({})
);

class HeaderImpl extends React.PureComponent<IHeaderImplProps, IHeaderState> {
  render() {
    return (
      <Sizes
        refs={["header", "footer"]}
        width={this.props.width}
        height={this.props.height}
      >
        {(sizes, refs) => (
          <>
            <div ref={refs.header as React.RefObject<HTMLDivElement>}>
              <Navbar color="white" light className="border-bottom">
                <Button
                  color={!this.props.sidebarOpen ? "primary" : "secondary"}
                  onClick={toggleSidebar}
                  className="mr-3"
                >
                  <Octicon icon={ThreeBars} />
                </Button>
                <Brand />
                <Nav className="mr-auto" />
                {this.props.isSyncing && (
                  <div
                    className="spinner-border text-secondary mr-2"
                    role="status"
                  >
                    <span className="sr-only">Loading...</span>
                  </div>
                )}
              </Navbar>
            </div>
            <div
              style={{
                overflowX: "hidden",
                overflowY: "scroll",
                width: this.props.width,
                height:
                  this.props.height -
                    (sizes.header.height + sizes.footer.height) || 0
              }}
            >
              <Row className="h-100 justify-content-center">
                <Sidebar
                  isOpen={this.props.sidebarOpen}
                  className="border-right bg-white"
                >
                  <Nav navbar className="ml-4">
                    <NavItem>
                      <NavLink tag="a" href={router.paths.Connections()}>
                        <FormattedMessage id="app.page.connections" />
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink tag="a" href={router.paths.CronJobs()}>
                        <FormattedMessage id="app.page.cronJobs" />
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink tag="a" href={router.paths.Tables()}>
                        <FormattedMessage id="app.page.tables" />
                      </NavLink>
                    </NavItem>
                  </Nav>
                </Sidebar>
                <Col>
                  <div className="mt-4 mb-4">{this.props.children}</div>
                </Col>
              </Row>
            </div>
            <Footer
              refObject={refs.footer as React.RefObject<HTMLDivElement>}
            />
          </>
        )}
      </Sizes>
    );
  }
}

export const Header = HeaderConnect(HeaderImpl);
