import { Option } from "@aicacia/core";
import { Record } from "immutable";
import { default as React } from "react";
import { FormattedMessage } from "react-intl";
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";
import { deleteConnection, IConnection } from "../../stores/connections";

export interface IDeleteConnectionProps {
  connection: Option<Record<IConnection>>;
  onClose(): void;
}

export interface IDeleteConnectionState {
  loading: boolean;
}

export class DeleteConnection extends React.PureComponent<
  IDeleteConnectionProps,
  IDeleteConnectionState
> {
  state: IDeleteConnectionState = {
    loading: false
  };
  onDelete = () => {
    this.props.connection.map(connection => {
      this.setState({ loading: true });
      deleteConnection(connection.get("id"))
        .then(this.props.onClose)
        .catch(() => {
          this.setState({ loading: false });
          this.props.onClose();
        });
    });
  };
  render() {
    return (
      <Modal isOpen={this.props.connection.isSome()}>
        <ModalHeader toggle={this.props.onClose}>
          {this.props.connection
            .map(connection => connection.get("name", ""))
            .unwrapOr("")}
        </ModalHeader>
        <ModalBody>
          <FormattedMessage
            id="connections.delete_model.message"
            values={{
              name: this.props.connection
                .map(connection => connection.get("name"))
                .unwrapOr("")
            }}
          />
        </ModalBody>
        <ModalFooter>
          <Button color="danger" onClick={this.onDelete}>
            <FormattedMessage id="connections.delete_model.confirm" />
          </Button>
          <Button onClick={this.props.onClose}>
            <FormattedMessage id="connections.delete_model.cancel" />
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}
