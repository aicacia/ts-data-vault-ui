import { IInputProps } from "@aicacia/state-forms/lib";
import { default as React } from "react";
import { Input, InputGroup, Label } from "reactstrap";
import { FormErrorMessage } from "../FormErrorMessage";

export type IGetSelectValue<T> = (value: T) => string | number;

export type ISelectFieldProps<T = any> = IInputProps<T> & {
  label?: JSX.Element;
  getSelectValue?: IGetSelectValue<T>;
};
export interface ISelectFieldState {}

export class SelectField<T = any> extends React.PureComponent<
  ISelectFieldProps<T>,
  ISelectFieldState
> {
  static defaultProps = {
    getSelectValue(value: any) {
      return value;
    }
  };

  render() {
    const {
      error,
      errors,
      children,
      value,
      label,
      getSelectValue,
      onChange,
      ...props
    } = this.props;

    return (
      <InputGroup>
        {label && <Label>{label}</Label>}
        <Input
          {...props}
          onChange={onChange as any}
          type="select"
          value={(getSelectValue as IGetSelectValue<T>)(value) as string}
          invalid={error}
        >
          {children}
        </Input>
        <FormErrorMessage errors={errors} />
      </InputGroup>
    );
  }
}
