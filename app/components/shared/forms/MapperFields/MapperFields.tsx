import Octicon, { Plus } from "@primer/octicons-react";
import { none, Option } from "@aicacia/core";
import { IInputProps } from "@aicacia/state-forms";
import { Record } from "immutable";
import { default as React } from "react";
import { Button, Col, FormGroup, Input, Row } from "reactstrap";
import { IMapperFieldJSON, MapperField } from "../../../../stores/mappers";
import { ITable } from "../../../../stores/tables";
import { FormErrorMessage } from "../FormErrorMessage";

export type IMapperFieldsProps = IInputProps<IMapperFieldJSON[]> & {
  table: Record<ITable>;
};

export class MapperFields extends React.PureComponent<IMapperFieldsProps> {
  addValue = () => this.props.change([...this.props.value, MapperField()]);
  createOnChange = (
    index: number,
    key: keyof IMapperFieldJSON,
    targetKey: string = "value"
  ) => (
    e: React.ChangeEvent<
      HTMLInputElement | HTMLTextAreaElement | HTMLSelectElement
    >
  ) => this.onChange(index, key, (e as any).target[targetKey]);
  createChange = (index: number, key: keyof IMapperFieldJSON) => (
    value: string
  ) => this.change(index, key, value);
  render() {
    const { value, errors } = this.props;

    return (
      <>
        <div>
          {value.map((mapperField, index) => (
            <>
              <FormGroup>
                <Input
                  value={mapperField.from}
                  disabled
                  onChange={this.createOnChange(index, "from")}
                />
              </FormGroup>
              <FormGroup>
                <Input
                  value={mapperField.to_id}
                  type="select"
                  disabled
                  onChange={this.createOnChange(index, "to_id")}
                >
                  {this.props.table.get("fields").map(tableField => (
                    <option
                      key={tableField.get("id")}
                      value={tableField.get("id")}
                    >
                      {tableField.get("name")}
                    </option>
                  ))}
                </Input>
              </FormGroup>
              <FormErrorMessage errors={errors} />
              <hr />
            </>
          ))}
        </div>
        <Row>
          <Col>
            <Button
              className="float-right"
              color="success"
              onClick={this.addValue}
            >
              <Octicon icon={Plus} />
            </Button>
          </Col>
        </Row>
      </>
    );
  }
  private onChange(index: number, key: keyof IMapperFieldJSON, value: any) {
    this.change(index, key, value);
  }
  private change(index: number, key: keyof IMapperFieldJSON, value: any) {
    const newValue = this.props.value.slice();
    newValue[index] = {
      ...value[index],
      [key]: value
    };
    this.props.change(newValue);
  }
}
