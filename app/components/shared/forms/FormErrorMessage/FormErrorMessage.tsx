import { IChangesetError } from "@aicacia/changeset";
import { List, Record } from "immutable";
import { default as React } from "react";
import { injectIntl, IntlShape } from "react-intl";
import { FormFeedback } from "reactstrap";

export interface IFormErrorMessageProps {
  intl: IntlShape;
  errors: List<Record<IChangesetError>>;
  translationScope?: string;
}
export interface IFormErrorMessageState {}

class FormErrorMessageImpl extends React.PureComponent<
  IFormErrorMessageProps,
  IFormErrorMessageState
> {
  static defaultProps = {
    translationScope: "error"
  };

  render() {
    return this.props.errors.map((error, index) => {
      const meta = error.get("meta");
      let message = error.get("message");

      if (!meta || meta.translate) {
        message = this.props.intl.formatMessage(
          { id: `${this.props.translationScope}.${message}` },
          { value: error.get("values").join(" ") }
        );
      }

      return (
        <FormFeedback type="invalid" key={index}>
          {message}
        </FormFeedback>
      );
    });
  }
}

export const FormErrorMessage = injectIntl(FormErrorMessageImpl);
