import Octicon, { Plus } from "@primer/octicons-react";
import { IInputProps } from "@aicacia/state-forms";
import { default as React } from "react";
import { FormattedMessage } from "react-intl";
import {
  Button,
  Col,
  FormGroup,
  Input,
  InputGroup,
  Label,
  Row
} from "reactstrap";
import { TABLE_TYPES } from "../../../../stores/shared";
import { ITableFieldJSON, TableField } from "../../../../stores/tables";
import { FormErrorMessage } from "../FormErrorMessage";

export type ITableFieldsProps = IInputProps<ITableFieldJSON[]>;

export class TableFields extends React.PureComponent<ITableFieldsProps> {
  addValue = () => this.props.change([...this.props.value, TableField()]);
  createOnChange = (
    index: number,
    key: keyof ITableFieldJSON,
    targetKey: string = "value"
  ) => (
    e: React.ChangeEvent<
      HTMLInputElement | HTMLTextAreaElement | HTMLSelectElement
    >
  ) => this.onChange(index, key, (e as any).target[targetKey]);
  createChange = (index: number, key: keyof ITableFieldJSON) => (
    value: string
  ) => this.change(index, key, value);
  render() {
    const { value, errors } = this.props;

    return (
      <>
        <div>
          {value.map((tableField, index) => (
            <>
              <FormGroup>
                <Input
                  value={tableField.name}
                  disabled
                  onChange={this.createOnChange(index, "name")}
                />
              </FormGroup>
              <FormGroup>
                <Input
                  value={tableField.type}
                  type="select"
                  disabled
                  onChange={this.createOnChange(index, "type")}
                >
                  {TABLE_TYPES.map(type => (
                    <option key={type} value={type}>
                      :{type}
                    </option>
                  ))}
                </Input>
              </FormGroup>
              <FormGroup tag="fieldset">
                <FormGroup check>
                  <Label check>
                    <Input
                      checked={tableField.tracked}
                      type="checkbox"
                      disabled
                      onChange={this.createOnChange(
                        index,
                        "tracked",
                        "checked"
                      )}
                    />
                    <FormattedMessage id="tables.forms.edit.tracked" />
                  </Label>
                </FormGroup>
                <FormGroup check>
                  <Label check>
                    <Input
                      checked={tableField.primary_key}
                      type="checkbox"
                      disabled
                      onChange={this.createOnChange(
                        index,
                        "primary_key",
                        "checked"
                      )}
                    />
                    <FormattedMessage id="tables.forms.edit.primaryKey" />
                  </Label>
                </FormGroup>
              </FormGroup>
              <FormErrorMessage errors={errors} />
              <hr />
            </>
          ))}
        </div>
        <Row>
          <Col>
            <Button
              className="float-right"
              color="success"
              onClick={this.addValue}
            >
              <Octicon icon={Plus} />
            </Button>
          </Col>
        </Row>
      </>
    );
  }
  private onChange(index: number, key: keyof ITableFieldJSON, value: any) {
    this.change(index, key, value);
  }
  private change(index: number, key: keyof ITableFieldJSON, value: any) {
    const newValue = this.props.value.slice();
    newValue[index] = {
      ...value[index],
      [key]: value
    };
    this.props.change(newValue);
  }
}
