import { ChangesetError } from "@aicacia/changeset";
import { IInjectedFormProps } from "@aicacia/state-forms";
import { default as React } from "react";
import { FormattedMessage } from "react-intl";
import { Button, Form } from "reactstrap";
import { state } from "../../../lib";
import { flattenErrors, IErrorJSON } from "../../../lib/utils";
import { updateCronJob } from "../../../stores/cronJobs";
import { injectForm, selectErrors } from "../../../stores/lib/forms";
import { FormErrorMessage } from "../forms/FormErrorMessage";
import { TextField } from "../forms/TextField";

export interface IEditCronJobFormValues {
  name: string;
  schedule: string;
}

export interface IEditCronJobFormProps
  extends IInjectedFormProps<IEditCronJobFormValues> {
  cronJobId: number;
  onClose(): void;
}

export interface IEditCronJobFormState {
  loading: boolean;
}

const EditCronJobFormInjectForm = injectForm<IEditCronJobFormValues>({
  changeset: changeset => changeset.validateRequired(["name", "schedule"])
});

class EditCronJobFormImpl extends React.PureComponent<IEditCronJobFormProps> {
  state: IEditCronJobFormState = {
    loading: false
  };
  onSubmit = (e: React.FormEvent) => {
    e.preventDefault();

    if (this.props.valid) {
      const formData = this.props.getFormData();

      this.setState({ loading: true });
      updateCronJob(this.props.cronJobId, formData.toJS())
        .then(this.props.onClose)
        .catch(error => {
          flattenErrors<IEditCronJobFormValues>(error).forEach(
            ([field, error]) => {
              this.props.addFieldError(
                field,
                ChangesetError({ message: error, meta: { translate: false } })
              );
            }
          );
        })
        .then(() => {
          this.setState({ loading: false });
        });
    }
  };
  render() {
    const { Field } = this.props;

    return (
      <Form onSubmit={this.onSubmit}>
        <Field name="name" Component={TextField} />
        <Field name="schedule" Component={TextField} />
        <Button
          type="submit"
          disabled={!this.props.valid || this.state.loading}
          onSubmit={this.onSubmit}
        >
          <FormattedMessage id="cronJobs.forms.edit.submit" />
        </Button>
        <FormErrorMessage
          errors={selectErrors(state.getState(), this.props.getFormId())}
        />
      </Form>
    );
  }
}

export const EditCronJobForm = EditCronJobFormInjectForm(EditCronJobFormImpl);
