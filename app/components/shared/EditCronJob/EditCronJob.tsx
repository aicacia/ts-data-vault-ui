import { Option } from "@aicacia/core";
import { Record } from "immutable";
import { default as React } from "react";
import { Modal, ModalBody, ModalHeader } from "reactstrap";
import { ICronJob } from "../../../stores/cronJobs";
import { EditCronJobForm } from "./EditCronJobForm";

export interface IEditCronJobProps {
  cronJob: Option<Record<ICronJob>>;
  onClose(): void;
}

export class EditCronJob extends React.PureComponent<IEditCronJobProps> {
  render() {
    return (
      <Modal isOpen={this.props.cronJob.isSome()}>
        <ModalHeader toggle={this.props.onClose}>
          {this.props.cronJob
            .map(cronJob => cronJob.get("name", ""))
            .unwrapOr("")}
        </ModalHeader>
        <ModalBody>
          {this.props.cronJob
            .map(cronJob => (
              <EditCronJobForm
                cronJobId={cronJob.get("id")}
                onClose={this.props.onClose}
                defaults={cronJob.toJS()}
              />
            ))
            .unwrapOr(null as any)}
        </ModalBody>
      </Modal>
    );
  }
}
