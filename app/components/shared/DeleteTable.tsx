import { Option } from "@aicacia/core";
import { Record } from "immutable";
import { default as React } from "react";
import { FormattedMessage } from "react-intl";
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";
import { deleteTable, ITable } from "../../stores/tables";

export interface IDeleteTableProps {
  table: Option<Record<ITable>>;
  onClose(): void;
}

export interface IDeleteTableState {
  loading: boolean;
}

export class DeleteTable extends React.PureComponent<
  IDeleteTableProps,
  IDeleteTableState
> {
  state: IDeleteTableState = {
    loading: false
  };
  onDelete = () => {
    this.props.table.map(table => {
      this.setState({ loading: true });
      deleteTable(table.get("id"))
        .then(this.props.onClose)
        .catch(() => {
          this.setState({ loading: false });
          this.props.onClose();
        });
    });
  };
  render() {
    return (
      <Modal isOpen={this.props.table.isSome()}>
        <ModalHeader toggle={this.props.onClose}>
          {this.props.table.map(table => table.get("name", "")).unwrapOr("")}
        </ModalHeader>
        <ModalBody>
          <FormattedMessage
            id="tables.delete_model.message"
            values={{
              name: this.props.table
                .map(table => table.get("name"))
                .unwrapOr("")
            }}
          />
        </ModalBody>
        <ModalFooter>
          <Button color="danger" onClick={this.onDelete}>
            <FormattedMessage id="tables.delete_model.confirm" />
          </Button>
          <Button onClick={this.props.onClose}>
            <FormattedMessage id="tables.delete_model.cancel" />
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}
