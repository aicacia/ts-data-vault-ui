import { ChangesetError } from "@aicacia/changeset";
import { IInjectedFormProps } from "@aicacia/state-forms";
import { List, Map } from "immutable";
import { default as React } from "react";
import { FormattedMessage } from "react-intl";
import { Button, Form } from "reactstrap";
import { state } from "../../../lib";
import { flattenErrors } from "../../../lib/utils";
import { updateExtractor } from "../../../stores/extractors";
import { injectForm, selectErrors } from "../../../stores/lib/forms";
import { FormErrorMessage } from "../forms/FormErrorMessage";
import { TextField } from "../forms/TextField";

export interface IEditPostgresExtractorFormValues {
  query: string;
}

export interface IEditPostgresExtractorFormProps
  extends IInjectedFormProps<IEditPostgresExtractorFormValues> {
  extractorId: number;
  connectionId: number;
  onClose(): void;
}

export interface IEditPostgresExtractorFormState {
  loading: boolean;
}

const EditPostgresExtractorFormInjectForm = injectForm<
  IEditPostgresExtractorFormValues
>({
  changeset: changeset => changeset.validateRequired(["query"])
});

class EditPostgresExtractorFormImpl extends React.PureComponent<
  IEditPostgresExtractorFormProps
> {
  state: IEditPostgresExtractorFormState = {
    loading: false
  };
  onSubmit = (e: React.FormEvent) => {
    e.preventDefault();

    if (this.props.valid) {
      const formData = this.props.getFormData();

      this.setState({ loading: true });
      updateExtractor(
        this.props.connectionId,
        this.props.extractorId,
        formData.toJS()
      )
        .then(this.props.onClose)
        .catch(error => {
          flattenErrors<IEditPostgresExtractorFormValues>(error).forEach(
            ([field, error]) => {
              this.props.addFieldError(
                field,
                ChangesetError({ message: error, meta: { translate: false } })
              );
            }
          );
        })
        .then(() => {
          this.setState({ loading: false });
        });
    }
  };
  render() {
    const { Field } = this.props;

    return (
      <Form onSubmit={this.onSubmit}>
        <Field name="query" type="textarea" Component={TextField} />
        <Button
          type="submit"
          disabled={!this.props.valid || this.state.loading}
          onSubmit={this.onSubmit}
        >
          <FormattedMessage id="extractors.forms.edit.submit" />
        </Button>
        <FormErrorMessage
          errors={selectErrors(state.getState(), this.props.getFormId())}
        />
      </Form>
    );
  }
}

export const EditPostgresExtractorForm = EditPostgresExtractorFormInjectForm(
  EditPostgresExtractorFormImpl
);
