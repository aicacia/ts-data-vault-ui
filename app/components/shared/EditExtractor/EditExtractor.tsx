import { none, Option, some } from "@aicacia/core";
import { Record } from "immutable";
import { default as React } from "react";
import { Modal, ModalBody, ModalHeader } from "reactstrap";
import { IExtractor, isPostgresqlExtractor } from "../../../stores/extractors";
import { EditPostgresExtractorForm } from "./EditPostgresExtractorForm";

export interface IEditExtractorProps {
  extractor: Option<Record<IExtractor>>;
  onClose(): void;
}

export class EditExtractor extends React.PureComponent<IEditExtractorProps> {
  render() {
    return (
      <Modal isOpen={this.props.extractor.isSome()}>
        <ModalHeader toggle={this.props.onClose}>
          {this.props.extractor
            .map(extractor => extractor.get("name", ""))
            .unwrapOr("")}
        </ModalHeader>
        <ModalBody>
          {this.props.extractor
            .flatMap(extractor =>
              isPostgresqlExtractor(extractor)
                ? some(
                    <EditPostgresExtractorForm
                      extractorId={extractor.get("id")}
                      connectionId={extractor.get("connection_id")}
                      onClose={this.props.onClose}
                      defaults={extractor.toJS()}
                    />
                  )
                : none<JSX.Element>()
            )
            .unwrapOr(null as any)}
        </ModalBody>
      </Modal>
    );
  }
}
