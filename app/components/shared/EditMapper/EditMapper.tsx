import { Option } from "@aicacia/core";
import { List, Record } from "immutable";
import { default as React } from "react";
import { Modal, ModalBody, ModalHeader } from "reactstrap";
import { IMapper } from "../../../stores/mappers";
import { ITable } from "../../../stores/tables";
import { EditMapperForm } from "./EditMapperForm";

export interface IEditMapperProps {
  mapper: Option<Record<IMapper>>;
  connectionId: number;
  tables: List<Record<ITable>>;
  onClose(): void;
}

export class EditMapper extends React.PureComponent<IEditMapperProps> {
  render() {
    return (
      <Modal isOpen={this.props.mapper.isSome()}>
        <ModalHeader toggle={this.props.onClose}>
          {this.props.mapper.map(mapper => mapper.get("name", "")).unwrapOr("")}
        </ModalHeader>
        <ModalBody>
          {this.props.mapper
            .map(mapper => (
              <EditMapperForm
                mapperId={mapper.get("id")}
                extractorId={mapper.get("extractor_id")}
                connectionId={this.props.connectionId}
                tables={this.props.tables}
                onClose={this.props.onClose}
                defaults={mapper.toJS()}
              />
            ))
            .unwrapOr(null as any)}
        </ModalBody>
      </Modal>
    );
  }
}
