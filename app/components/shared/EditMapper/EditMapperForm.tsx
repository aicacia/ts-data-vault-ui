import { ChangesetError } from "@aicacia/changeset";
import { Option } from "@aicacia/core";
import { IInjectedFormProps } from "@aicacia/state-forms";
import { IInputProps } from "@aicacia/state-forms/lib";
import { List, Record } from "immutable";
import { default as React } from "react";
import { FormattedMessage } from "react-intl";
import { Button, Form } from "reactstrap";
import { state } from "../../../lib";
import { flattenErrors } from "../../../lib/utils";
import {
  injectForm,
  selectErrors,
  selectField
} from "../../../stores/lib/forms";
import { IMapperFieldJSON, updateMapper } from "../../../stores/mappers";
import { ITable } from "../../../stores/tables";
import { FormErrorMessage } from "../forms/FormErrorMessage";
import { MapperFields } from "../forms/MapperFields";
import { SelectField } from "../forms/SelectField";
import { TextField } from "../forms/TextField";

export interface IEditMapperFormValues {
  name: string;
  table_id: number;
  fields: IMapperFieldJSON[];
}

export interface IEditMapperFormProps
  extends IInjectedFormProps<IEditMapperFormValues> {
  mapperId: number;
  extractorId: number;
  connectionId: number;
  tables: List<Record<ITable>>;
  onClose(): void;
}

export interface IEditMapperFormState {
  loading: boolean;
}

const EditMapperFormInjectForm = injectForm<IEditMapperFormValues>({
  changeset: changeset =>
    changeset.validateRequired(["name", "table_id", "fields"])
});

class EditMapperFormImpl extends React.PureComponent<IEditMapperFormProps> {
  state: IEditMapperFormState = {
    loading: false
  };
  onSubmit = (e: React.FormEvent) => {
    e.preventDefault();

    if (this.props.valid) {
      const formData = this.props.getFormData();

      this.setState({ loading: true });
      updateMapper(
        this.props.connectionId,
        this.props.extractorId,
        this.props.mapperId,
        formData.toJS()
      )
        .then(this.props.onClose)
        .catch(error => {
          flattenErrors<IEditMapperFormValues>(error).forEach(
            ([field, error]) => {
              this.props.addFieldError(
                field,
                ChangesetError({ message: error, meta: { translate: false } })
              );
            }
          );
        })
        .then(() => {
          this.setState({ loading: false });
        });
    }
  };
  render() {
    const { Field } = this.props,
      tableId = +selectField(
        state.getState(),
        this.props.getFormId(),
        "table_id"
      ).get("value", 0);

    return (
      <Form onSubmit={this.onSubmit}>
        <Field name="name" disabled Component={TextField} />
        <Field name="table_id" Component={SelectField}>
          {this.props.tables.map(table => (
            <option key={table.get("id")} value={table.get("id")}>
              {table.get("name")}
            </option>
          ))}
        </Field>
        {Option.from(
          this.props.tables.find(table => table.get("id") === tableId)
        )
          .map(table => (
            <Field name="fields" table={table} Component={MapperFields} />
          ))
          .unwrapOr(null as any)}
        <Button
          type="submit"
          disabled={!this.props.valid || this.state.loading}
          onSubmit={this.onSubmit}
        >
          <FormattedMessage id="mappers.forms.edit.submit" />
        </Button>
        <FormErrorMessage
          errors={selectErrors(state.getState(), this.props.getFormId())}
        />
      </Form>
    );
  }
}

export const EditMapperForm = EditMapperFormInjectForm(EditMapperFormImpl);
