import { Option } from "@aicacia/core";
import { Record } from "immutable";
import { default as React } from "react";
import { Modal, ModalBody, ModalHeader } from "reactstrap";
import { ITable } from "../../../stores/tables";
import { EditTableForm } from "./EditTableForm";

export interface IEditTableProps {
  table: Option<Record<ITable>>;
  onClose(): void;
}

export class EditTable extends React.PureComponent<IEditTableProps> {
  render() {
    return (
      <Modal isOpen={this.props.table.isSome()}>
        <ModalHeader toggle={this.props.onClose}>
          {this.props.table.map(table => table.get("name", "")).unwrapOr("")}
        </ModalHeader>
        <ModalBody>
          {this.props.table
            .map(table => (
              <EditTableForm
                tableId={table.get("id")}
                onClose={this.props.onClose}
                defaults={table.toJS()}
              />
            ))
            .unwrapOr(null as any)}
        </ModalBody>
      </Modal>
    );
  }
}
