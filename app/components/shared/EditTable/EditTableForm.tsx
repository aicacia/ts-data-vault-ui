import { ChangesetError } from "@aicacia/changeset";
import { IInjectedFormProps } from "@aicacia/state-forms";
import { IInputProps } from "@aicacia/state-forms/lib";
import { List } from "immutable";
import { default as React } from "react";
import { FormattedMessage } from "react-intl";
import { Button, Form } from "reactstrap";
import { state } from "../../../lib";
import { flattenErrors } from "../../../lib/utils";
import { injectForm, selectErrors } from "../../../stores/lib/forms";
import { ITableFieldJSON, updateTable } from "../../../stores/tables";
import { FormErrorMessage } from "../forms/FormErrorMessage";
import { TableFields } from "../forms/TableFields";
import { TextField } from "../forms/TextField";

export interface IEditTableFormValues {
  name: string;
  fields: ITableFieldJSON[];
}

export interface IEditTableFormProps
  extends IInjectedFormProps<IEditTableFormValues> {
  tableId: number;
  onClose(): void;
}

export interface IEditTableFormState {
  loading: boolean;
}

const EditTableFormInjectForm = injectForm<IEditTableFormValues>({
  changeset: changeset => changeset.validateRequired(["name", "fields"])
});

class EditTableFormImpl extends React.PureComponent<IEditTableFormProps> {
  state: IEditTableFormState = {
    loading: false
  };
  onSubmit = (e: React.FormEvent) => {
    e.preventDefault();

    if (this.props.valid) {
      const formData = this.props.getFormData();

      this.setState({ loading: true });
      updateTable(this.props.tableId, formData.toJS())
        .then(this.props.onClose)
        .catch(error => {
          flattenErrors<IEditTableFormValues>(error).forEach(
            ([field, error]) => {
              this.props.addFieldError(
                field,
                ChangesetError({ message: error, meta: { translate: false } })
              );
            }
          );
        })
        .then(() => {
          this.setState({ loading: false });
        });
    }
  };
  render() {
    const { Field } = this.props;

    return (
      <Form onSubmit={this.onSubmit}>
        <Field name="name" disabled Component={TextField} />
        <Field name="fields" Component={TableFields} />
        <Button
          type="submit"
          disabled={!this.props.valid || this.state.loading}
          onSubmit={this.onSubmit}
        >
          <FormattedMessage id="tables.forms.edit.submit" />
        </Button>
        <FormErrorMessage
          errors={selectErrors(state.getState(), this.props.getFormId())}
        />
      </Form>
    );
  }
}

export const EditTableForm = EditTableFormInjectForm(EditTableFormImpl);
