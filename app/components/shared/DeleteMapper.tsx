import { Option } from "@aicacia/core";
import { Record } from "immutable";
import { default as React } from "react";
import { FormattedMessage } from "react-intl";
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";
import { deleteMapper, IMapper } from "../../stores/mappers";

export interface IDeleteMapperProps {
  mapper: Option<Record<IMapper>>;
  connectionId: number;
  onClose(): void;
}

export interface IDeleteMapperState {
  loading: boolean;
}

export class DeleteMapper extends React.PureComponent<
  IDeleteMapperProps,
  IDeleteMapperState
> {
  state: IDeleteMapperState = {
    loading: false
  };
  onDelete = () => {
    this.props.mapper.map(mapper => {
      this.setState({ loading: true });
      deleteMapper(
        this.props.connectionId,
        mapper.get("extractor_id"),
        mapper.get("id")
      )
        .then(this.props.onClose)
        .catch(() => {
          this.setState({ loading: false });
          this.props.onClose();
        });
    });
  };
  render() {
    return (
      <Modal isOpen={this.props.mapper.isSome()}>
        <ModalHeader toggle={this.props.onClose}>
          {this.props.mapper.map(mapper => mapper.get("name", "")).unwrapOr("")}
        </ModalHeader>
        <ModalBody>
          <FormattedMessage
            id="mappers.delete_model.message"
            values={{
              name: this.props.mapper
                .map(mapper => mapper.get("name"))
                .unwrapOr("")
            }}
          />
        </ModalBody>
        <ModalFooter>
          <Button color="danger" onClick={this.onDelete}>
            <FormattedMessage id="mappers.delete_model.confirm" />
          </Button>
          <Button onClick={this.props.onClose}>
            <FormattedMessage id="mappers.delete_model.cancel" />
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}
