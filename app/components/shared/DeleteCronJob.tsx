import { Option } from "@aicacia/core";
import { Record } from "immutable";
import { default as React } from "react";
import { FormattedMessage } from "react-intl";
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";
import { deleteCronJob, ICronJob } from "../../stores/cronJobs";

export interface IDeleteCronJobProps {
  cronJob: Option<Record<ICronJob>>;
  onClose(): void;
}

export interface IDeleteCronJobState {
  loading: boolean;
}

export class DeleteCronJob extends React.PureComponent<
  IDeleteCronJobProps,
  IDeleteCronJobState
> {
  state: IDeleteCronJobState = {
    loading: false
  };
  onDelete = () => {
    this.props.cronJob.map(cronJob => {
      this.setState({ loading: true });
      deleteCronJob(cronJob.get("id"))
        .then(this.props.onClose)
        .catch(() => {
          this.setState({ loading: false });
          this.props.onClose();
        });
    });
  };
  render() {
    return (
      <Modal isOpen={this.props.cronJob.isSome()}>
        <ModalHeader toggle={this.props.onClose}>
          {this.props.cronJob
            .map(cronJob => cronJob.get("name", ""))
            .unwrapOr("")}
        </ModalHeader>
        <ModalBody>
          <FormattedMessage
            id="cronJobs.delete_model.message"
            values={{
              name: this.props.cronJob
                .map(cronJob => cronJob.get("name"))
                .unwrapOr("")
            }}
          />
        </ModalBody>
        <ModalFooter>
          <Button color="danger" onClick={this.onDelete}>
            <FormattedMessage id="cronJobs.delete_model.confirm" />
          </Button>
          <Button onClick={this.props.onClose}>
            <FormattedMessage id="cronJobs.delete_model.cancel" />
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}
