import { none, Option, some } from "@aicacia/core";
import { Record } from "immutable";
import { default as React } from "react";
import { Modal, ModalBody, ModalHeader } from "reactstrap";
import {
  IConnection,
  isPostgresqlConnection
} from "../../../stores/connections";
import { EditPostgresConnectionForm } from "./EditPostgresConnectionForm";

export interface IEditConnectionProps {
  connection: Option<Record<IConnection>>;
  onClose(): void;
}

export class EditConnection extends React.PureComponent<IEditConnectionProps> {
  render() {
    return (
      <Modal isOpen={this.props.connection.isSome()}>
        <ModalHeader toggle={this.props.onClose}>
          {this.props.connection
            .map(connection => connection.get("name", ""))
            .unwrapOr("")}
        </ModalHeader>
        <ModalBody>
          {this.props.connection
            .flatMap(connection =>
              isPostgresqlConnection(connection)
                ? some(
                    <EditPostgresConnectionForm
                      connectionId={connection.get("id")}
                      onClose={this.props.onClose}
                      defaults={connection.toJS()}
                    />
                  )
                : none<JSX.Element>()
            )
            .unwrapOr(null as any)}
        </ModalBody>
      </Modal>
    );
  }
}
