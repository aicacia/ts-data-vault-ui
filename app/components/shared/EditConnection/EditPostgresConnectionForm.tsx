import { ChangesetError } from "@aicacia/changeset";
import { IInjectedFormProps } from "@aicacia/state-forms";
import { List, Map } from "immutable";
import { default as React } from "react";
import { FormattedMessage } from "react-intl";
import { Button, Form } from "reactstrap";
import { state } from "../../../lib";
import { flattenErrors } from "../../../lib/utils";
import { updateConnection } from "../../../stores/connections";
import { injectForm, selectErrors } from "../../../stores/lib/forms";
import { FormErrorMessage } from "../forms/FormErrorMessage";
import { TextField } from "../forms/TextField";

export interface IEditPostgresConnectionFormValues {
  uri: string;
  username: string;
  password: string;
}

export interface IEditPostgresConnectionFormProps
  extends IInjectedFormProps<IEditPostgresConnectionFormValues> {
  connectionId: number;
  onClose(): void;
}

export interface IEditPostgresConnectionFormState {
  loading: boolean;
}

const EditPostgresConnectionFormInjectForm = injectForm<
  IEditPostgresConnectionFormValues
>({
  changeset: changeset =>
    changeset.validateRequired(["uri", "username", "password"])
});

class EditPostgresConnectionFormImpl extends React.PureComponent<
  IEditPostgresConnectionFormProps
> {
  state: IEditPostgresConnectionFormState = {
    loading: false
  };
  onSubmit = (e: React.FormEvent) => {
    e.preventDefault();

    if (this.props.valid) {
      const formData = this.props.getFormData();

      this.setState({ loading: true });
      updateConnection(this.props.connectionId, formData.toJS())
        .then(this.props.onClose)
        .catch(error => {
          flattenErrors<IEditPostgresConnectionFormValues>(error).forEach(
            ([field, error]) => {
              this.props.addFieldError(
                field,
                ChangesetError({ message: error, meta: { translate: false } })
              );
            }
          );
        })
        .then(() => {
          this.setState({ loading: false });
        });
    }
  };
  render() {
    const { Field } = this.props;

    return (
      <Form onSubmit={this.onSubmit}>
        <Field name="uri" Component={TextField} />
        <Field name="username" Component={TextField} />
        <Field name="password" type="password" Component={TextField} />
        <Button
          type="submit"
          disabled={!this.props.valid || this.state.loading}
          onSubmit={this.onSubmit}
        >
          <FormattedMessage id="connections.forms.edit.submit" />
        </Button>
        <FormErrorMessage
          errors={selectErrors(state.getState(), this.props.getFormId())}
        />
      </Form>
    );
  }
}

export const EditPostgresConnectionForm = EditPostgresConnectionFormInjectForm(
  EditPostgresConnectionFormImpl
);
