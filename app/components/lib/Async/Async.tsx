import { SafeStatePureComponent } from "../SafeStatePureComponent";

export interface IAsyncProps<T> {
  promise?: Promise<T>;
  onSuccess(value: T): JSX.Element;
  onError(error: Error): JSX.Element;
  onPending(): JSX.Element;
}
export interface IAsyncState<T> {
  promise?: Promise<void>;
  value?: T;
  error?: Error;
}

export class Async<T> extends SafeStatePureComponent<
  IAsyncProps<T>,
  IAsyncState<T>
> {
  constructor(props: IAsyncProps<T>) {
    super(props);

    this.state = {};
  }
  componentDidMount() {
    super.componentDidMount();

    if (this.props.promise) {
      this._initPromise(this.props.promise);
    }
  }
  componentDidUpdate(prev: IAsyncProps<T>) {
    if (prev.promise !== this.props.promise && this.props.promise) {
      this._initPromise(this.props.promise);
    }
  }
  _initPromise(promise: Promise<T>) {
    this.safeSetState({
      value: undefined,
      error: undefined,
      promise: promise
        .then(value => this.safeSetState({ value }))
        .catch(error => this.safeSetState({ error }))
    });
  }
  render() {
    const { error, value } = this.state,
      { onSuccess, onError, onPending } = this.props;

    if (error) {
      return onError(error);
    } else if (value) {
      return onSuccess(value);
    } else {
      return onPending();
    }
  }
}
