import { default as React } from "react";
import { IState, Provider, state } from "../../../lib/state";
import { SafeStatePureComponent } from "../SafeStatePureComponent";

export interface IStateProviderProps {}

export interface IStateProviderStateProvider {
  value: IState;
}

export class StateProvider extends SafeStatePureComponent<
  IStateProviderProps,
  IStateProviderStateProvider
> {
  private _isUpdating: boolean = false;

  constructor(props: IStateProviderProps) {
    super(props);

    this.state = {
      value: state.getState()
    };
  }

  onSetState = () => {
    if (!this._isUpdating) {
      this._isUpdating = true;
      process.nextTick(this.runSetState);
    }
  };

  runSetState = () => {
    this._isUpdating = false;
    this.safeSetState({ value: state.getState() });
  };

  componentDidMount() {
    super.componentDidMount();
    state.addListener("set-state", this.onSetState);
  }

  componentWillUnmount() {
    super.componentWillUnmount();
    state.removeListener("set-state", this.onSetState);
  }

  render() {
    return <Provider value={this.state.value}>{this.props.children}</Provider>;
  }
}
