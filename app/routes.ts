import { ConnectionsPage } from "./components/pages/Connections/ConnectionsPage";
import { CronJobsPage } from "./components/pages/CronJobs/CronJobsPage";
import { Error404Page } from "./components/pages/Error404/Error404Page";
import { Error500Page } from "./components/pages/Error500/Error500Page";
import { ExtractorsPage } from "./components/pages/Extractors/ExtractorsPage";
import { IndexPage } from "./components/pages/Index/IndexPage";
import { MappersPage } from "./components/pages/Mappers/MappersPage";
import { PrivacyPolicyPage } from "./components/pages/PrivacyPolicy/PrivacyPolicyPage";
import { TablesPage } from "./components/pages/Tables/TablesPage";
import { TermsPage } from "./components/pages/Terms/TermsPage";
import { renderApp } from "./lib/internal";
import { router } from "./lib/router";
import { showConnectionHandler } from "./middleware/showConnectionHandler";

const init = () => {
  router.page("Error404", "/404", Error404Page);
  router.page("Error500", "/500", Error500Page);

  /* DO NOT REMOVE THIS */
  router.page("Index", "/", IndexPage);

  router.with("/tables", scope => {
    scope.page("Tables", "/", TablesPage);
  });
  router.with("/cron-jobs", scope => {
    scope.page("CronJobs", "/", CronJobsPage);
  });
  router.with("/connections", connections => {
    connections.page("Connections", "/", ConnectionsPage);

    connections.with("/:connection_id", connection => {
      connection.use("/", showConnectionHandler);

      connection.with("/extractors", extractors => {
        extractors.page("Extractors", "/", ExtractorsPage);

        extractors.with("/:extractor_id", extractor => {
          extractor.with("/mappers", mappers => {
            mappers.page("Mappers", "/", MappersPage);
          });
        });
      });
    });
  });

  router.page("PrivacyPolicy", "/privacy_policy", PrivacyPolicyPage);
  router.page("Terms", "/terms", TermsPage);
};

init();

if (process.env.NODE_ENV !== "production") {
  if ((module as any).hot) {
    ((module as any).hot as any).accept(() => {
      router.clear();
      init();
      renderApp();
    });
  }
}
